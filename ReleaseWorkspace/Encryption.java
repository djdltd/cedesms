/*
 * Encryption.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.io.Base64InputStream;
import java.io.*;
import java.util.*;
import customjava.security.SecureRandom;
import customjava.math.BigInteger;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.engines.RSAEngine;

/**
 * 
 */
class Encryption {
    
    private static Random _rnd = new Random();
    public static String _privatekeymod = "";
    public static String _privatekeyexp = "";
    public static String _publickeymod = "";
    public static String _publickeyexp = "";
    
    Encryption() {    
    
    }
    
    public static void RSAGenerateKeys ()
    {
        RSAKeyPairGenerator  pGen = new RSAKeyPairGenerator();
        RSAKeyGenerationParameters  genParam = new RSAKeyGenerationParameters(BigInteger.valueOf(0x11), new SecureRandom(), 128, 25);

        pGen.init(genParam);
        
        AsymmetricCipherKeyPair  pair = pGen.generateKeyPair();
        
        // Get the public key
        RSAKeyParameters paramPublic = (RSAKeyParameters) pair.getPublic();            
        BigInteger biMod = paramPublic.getModulus();
        BigInteger biExp = paramPublic.getExponent();
        
        // Make it global
        _publickeymod = biMod.toString();
        _publickeyexp = biExp.toString();
        
        // Get the private key
        RSAKeyParameters paramPrivate = (RSAKeyParameters) pair.getPrivate();
        BigInteger biModprivate = paramPrivate.getModulus();
        BigInteger biExpprivate = paramPrivate.getExponent();
        
        // Make it global
        _privatekeymod = biModprivate.toString();
        _privatekeyexp = biExpprivate.toString();               
    }
    
    public static String RSAEncrypt (String strModulus, String strExponent, String strPlaintext)
    {
        // RSA encrypt some plaintext given the modulus and exponent of the public key.
        byte[] bData = strPlaintext.getBytes();
        String strBase64 = "";
                
        RSAKeyParameters paramNewPublic = new RSAKeyParameters(false, new BigInteger(strModulus), new BigInteger(strExponent));
        
        RSAEngine eng = new RSAEngine();
        
        eng.init(true, paramNewPublic);
        
        bData = eng.processBlock(bData, 0, bData.length);
        
        // Now return the encrypted string encoded using Base64
        strBase64 = ToBase64(bData, bData.length);
        
        return strBase64;
    }
    
    public static String RSADecrypt (String strModulus, String strExponent, String strCiphertext)
    {
        // RSA Decrypt some ciphertext given the modulus and exponent of the private key
        String strBase64 = strCiphertext;        
        String strDecrypted = "";
        byte[] bData = FromBase64(strBase64);
        
        RSAKeyParameters paramNewPrivate = new RSAKeyParameters(true, new BigInteger(strModulus), new BigInteger(strExponent));
        
        RSAEngine eng = new RSAEngine();
                
        eng.init(false, paramNewPrivate);
        
        bData = eng.processBlock(bData, 0, bData.length);
        
        strDecrypted = new String(bData);
        
        return strDecrypted;
    }
    
    public static String EncryptText (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 512); // First generate the key needed from the password
        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlaintext.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        String strBase64 = "";
        int curTrans = 0;
        
        for (b=0;b<strPlaintext.length();b++)
        {
            strCurtrans = strKey.substring(b, b+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte+=curTrans;
            
            bEnctext[b] = curByte;
        }
        
        strEncrypted = new String(bEnctext);
                        
        strBase64 = ToBase64(bEnctext, strEncrypted.length());        
        
        return strBase64;
    }

    public static  int GetRand (int Max)
    {
        return _rnd.nextInt(Max);
    }

    public static  String EncryptTextEx (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 512); // First generate the key needed from the password
        String strPlainex = strPlaintext + "0";
        
        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlainex.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;
        
        int rand = GetRand(47);
        byte brand = (byte) rand;
        
        String strCurtrans = "";
        String strEncrypted = "";
        String strBase64 = "";
        int curTrans = 0;
        
        for (b=0;b<strPlaintext.length();b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte+=curTrans;
            
            bEnctext[b+1] = curByte;
        }
        
        bEnctext[0] = brand;
        strEncrypted = new String(bEnctext);
        strBase64 = ToBase64(bEnctext, strEncrypted.length());        
        
        return strBase64;
    }

    public static String EncryptTextStrong (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 2048); // First generate the key needed from the password
        String strPlainex = strPlaintext + "0";
        String strEncryptedlayer;
        
        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlainex.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;
        
        int rand = GetRand(47);
        byte brand = (byte) rand;
        
        String strCurtrans = "";
        String strEncrypted = "";
        String strBase64 = "";
        int curTrans = 0;
        
        for (b=0;b<strPlaintext.length();b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];
            curByte+=curTrans;
            
            bEnctext[b+1] = curByte;
        }
                
        bEnctext[0] = brand;
        strEncrypted = new String(bEnctext);
        
        for (b=0;b<(strKey.length()-strEncrypted.length()-3);b++) {
            strEncryptedlayer = AddCipherLayer(strKey, b, strEncrypted, true);
            strEncrypted = strEncryptedlayer;
        }
 
         
        //strEncryptedlayer = AddCipherLayer(strKey, 10, strEncrypted, true);
        //strEncrypted = strEncryptedlayer;

        bEnctext = strEncrypted.getBytes();
        
        strBase64 = ToBase64(bEnctext, strEncrypted.length());        
        
        return strBase64;
    }

    public static String DecryptTextStrong (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 2048); // First generate the key needed from the password
        //String strFrombase64 = FromBase64(strPlaintext);
        //byte[] bPlaintext = strFrombase64.getBytes(); // Get the bytes of the string
        byte[] bPlaintext = FromBase64(strPlaintext);
        //byte[] bEnctext = strFrombase64.getBytes(); // Get the bytes of the string for encoding
        byte[] bEnctext = bPlaintext;
        
        //int rand = bPlaintext[0];
        int rand;
        
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        int curTrans = 0;
        String strDecryptedlayer;
        String cipherText = new String(bEnctext);

        
        for (b=0;b<(strKey.length()-cipherText.length()-3);b++) {
            strDecryptedlayer = AddCipherLayer(strKey, (strKey.length()-cipherText.length()-3)-b-1, cipherText, false);
            cipherText = strDecryptedlayer;
        }
        
                
        //strDecryptedlayer = AddCipherLayer(strKey, 10, cipherText, false);
        //cipherText = strDecryptedlayer;                
        
        bPlaintext = cipherText.getBytes(); // Get the bytes of the unlayered ciphertext
        rand = bPlaintext[0];                        
                        
        for (b=0;b<bPlaintext.length-1;b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b+1];            
            curByte-=curTrans;
            
            bEnctext[b] = curByte;
        }
        
        strEncrypted = new String(bEnctext);
        strEncrypted = strEncrypted.substring(0, strEncrypted.length()-1);
        
        return strEncrypted;
    }
    
    public static String AddCipherLayer (String strKey, int keyoffset, String strPlaintext, boolean encrypt)
    {
        int b = 0;        
        String strPlainex = strPlaintext;
        
        byte[] bPlaintext = strPlaintext.getBytes(); // Get the bytes of the string
        byte[] bEnctext = strPlainex.getBytes(); // Get the bytes of the string for encoding
        byte curByte = 0;
                        
        String strCurtrans = "";
        String strEncrypted = "";
        
        int curTrans = 0;              
        
        if (keyoffset <= (strKey.length()-strPlaintext.length()-3)) {        
            for (b=0;b<strPlaintext.length();b++)
            {
                strCurtrans = strKey.substring(b+keyoffset, b+keyoffset+3);
                curTrans = Integer.parseInt(strCurtrans);
                curByte = bPlaintext[b];
                
                if (encrypt == true) {
                    curByte+=curTrans;
                } else {
                    curByte-=curTrans;
                }
                            
                bEnctext[b] = curByte;
            }
        }
                
        strEncrypted = new String(bEnctext);               
        return strEncrypted;
    }

    public static String DecryptText (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 512); // First generate the key needed from the password
        //String strFrombase64 = FromBase64(strPlaintext);
        //byte[] bPlaintext = strFrombase64.getBytes(); // Get the bytes of the string
        byte[] bPlaintext = FromBase64(strPlaintext);
        //byte[] bEnctext = strFrombase64.getBytes(); // Get the bytes of the string for encoding
        byte[] bEnctext = bPlaintext;
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        int curTrans = 0;
        
        for (b=0;b<bPlaintext.length;b++)
        {
            strCurtrans = strKey.substring(b, b+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b];            
            curByte-=curTrans;
            
            bEnctext[b] = curByte;
        }
        
        strEncrypted = new String(bEnctext);
        
        return strEncrypted;
    }

    public static String DecryptTextEx (String strPassword, String strPlaintext)
    {
        int b = 0;
        String strKey = GenerateKey(strPassword, 512); // First generate the key needed from the password
        //String strFrombase64 = FromBase64(strPlaintext);
        //byte[] bPlaintext = strFrombase64.getBytes(); // Get the bytes of the string
        byte[] bPlaintext = FromBase64(strPlaintext);
        //byte[] bEnctext = strFrombase64.getBytes(); // Get the bytes of the string for encoding
        byte[] bEnctext = bPlaintext;
        
        int rand = bPlaintext[0];
        
        byte curByte = 0;
        String strCurtrans = "";
        String strEncrypted = "";
        int curTrans = 0;
                        
        for (b=0;b<bPlaintext.length-1;b++)
        {
            strCurtrans = strKey.substring(b+rand, b+rand+3);
            curTrans = Integer.parseInt(strCurtrans);
            curByte = bPlaintext[b+1];            
            curByte-=curTrans;
            
            bEnctext[b] = curByte;
        }
        
        strEncrypted = new String(bEnctext);
        strEncrypted = strEncrypted.substring(0, strEncrypted.length()-1);
        
        return strEncrypted;
    }

    public static String GenerateKey (String strPassword, int keylength)
    {
        int a = 0;
        int curByte = 0;
        int bcurByte = 0;
        int lChecksum = 0;
        byte byteCurrent = 0;
        char charbyte;
        String strbyte = "";
        byte[] bPassword = strPassword.getBytes();
        String strNumerics = "";
        
        // Now we need to calculate a checksum on the numerics
        for (a=0;a<strPassword.length();a++)
        {
            curByte = bPassword[a];
            lChecksum+=curByte;                        
        }                
        
        for (a=0;a<strPassword.length();a++)
        {
            curByte = bPassword[a];
            curByte+=lChecksum;
            
            bcurByte = curByte % 100;                       
            bPassword[a] = (byte) bcurByte;           
        }
        
        for (a=0;a<strPassword.length();a++)
        {
            strNumerics+=String.valueOf(bPassword[a]);
        }
                
        strNumerics = RepeatExpand(strNumerics, keylength);
        
        return strNumerics;
    }

    public static long Raise (long lValue, long lPower)
    {
        long lRes = lValue;
        int l=0;
        for (l=1;l<lPower;l++)
        {
            lRes = lRes * lValue;
        }
        return lRes;
    }
    
    public static String ToBase64 (byte[] input, int length)
    {
        
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream( length );    
        Base64OutputStream base64OutputStream = new Base64OutputStream( byteArrayOutputStream );
        
        try 
        {
            base64OutputStream.write( input, 0, length );
            base64OutputStream.flush();
            base64OutputStream.close();  
        } 
        catch (IOException ioe) 
        {
            return null;
        }
        
        return byteArrayOutputStream.toString();    
    }
    
    public static byte[] FromBase64 (String strEncoded)
    {
        try
        {
            byte [] data = Base64InputStream.decode(strEncoded);
            //String decoded = new String(data, "UTF-8");
            return data;
            //return decoded;        
        }
        catch (IOException ioe)
        {
            return null;
        }
    }
    
    public static String Expand (String strInval)
    {
        //add(new RichTextField("Expand function called." ,Field.NON_FOCUSABLE));
        //String strInval = "237745";
        String cExp1 = "";
        String cExp2 = "";
        int c = 0;
        long lExp1;
        long lExp2;
        long lPower=0;
        String strResult = "";
        
        for (c=0;c<strInval.length ();c++)
        {
            if (c>0) 
            {
                cExp1 = strInval.substring(c-1,c);
                cExp2 = strInval.substring(c,c+1);
                lExp1 = Long.parseLong(cExp1);
                lExp2 = Long.parseLong(cExp2);
                
                lPower = Raise(lExp1, lExp2);
                
                strResult += Long.toString(lPower);
                //add(new RichTextField(cExp1 ,Field.NON_FOCUSABLE));
                //add(new RichTextField(cExp2 ,Field.NON_FOCUSABLE));
            }
        }
        
        //add(new RichTextField("Expand Value: "+strResult ,Field.NON_FOCUSABLE));
        return strResult;
    }

    public static String RepeatExpand (String strInval, int Length)
    {
        String strResult = "";
        String strNewResult = "";
        
        strResult = Expand(strInval);
        
        while (strResult.length() < Length)
        {
            strNewResult = Expand(strResult);
            strResult = strNewResult;
        }
        
        return strResult;
    }
    
    
    public static String Instantencrypt (String strPlaintext)
    {
        return EncryptTextEx("hdj48fjalxa12", strPlaintext);
    }
    
    public static String Instantdecrypt (String strCiphertext)
    {
        return DecryptTextEx("hdj48fjalxa12", strCiphertext);
    }
    
    public static boolean isOwnerinTCR (String strMessage)
    {
        String strFiltermessage = "";
        
        if (strMessage.startsWith("#RC") == true) {
            strFiltermessage = strMessage.substring(3);
        } else {
          return false;  
        }
        
        String strDec = Instantdecrypt(strFiltermessage);
        
        String strOwner = GetDataPart(strDec, 1, "^");
        
        if (strOwner.compareTo("&") == 0) {
            System.out.println("No TCR owner found!");
            return false;
        } else {
            System.out.println("TCR owner present.");
            return true;
        }
    }
    
    public static String getTCRowner(String strMessage)
    {
        String strFiltermessage = "";
        
        if (strMessage.startsWith("#RC") == true) {
            strFiltermessage = strMessage.substring(3);
        } else {
          return "A CedeSMS user";  
        }
        
        String strDec = Instantdecrypt(strFiltermessage);
        
        String strOwner = GetDataPart(strDec, 1, "^");
        
        String strFullname = GetDataPart(strOwner, 1, "&") + " " + GetDataPart(strOwner,2,"&");
        
        System.out.println("TCR Owner: " + strFullname.trim());
        
        return strFullname.trim();
        
    }
    
    public static String GetDataPart (String datal, int dpart, String sepChar)
    {
        int count;
        int sepcount = 0;
        String conString = "";
        String Inline;
        
        Inline = datal;
        Inline = Inline + sepChar;
        
        for (count=0;count<Inline.length();count++) {
            if (Inline.substring(count, count+1).compareTo(sepChar) != 0) {
                conString+=Inline.substring(count,count+1);
            } else {
                if (count > 0) {
                    if (Inline.substring((count-1), count).compareTo(sepChar) != 0) {
                        sepcount++;
                        if (sepcount == dpart) {
                            return conString;
                        } else {
                            conString = "";
                        }
                    }
                }
            }
        }
        
        return "";
    }
    
    public static int genRandomprintablechar()
    {
        // First generate a random number between 1 and 3
        int section = GetRand(3);
        int rchar = 0;
        
        if (section == 0) {
            rchar = 48+GetRand(9);
        }
        
        if (section == 1) {
            rchar = 65+GetRand(26);
        }
        
        if (section == 2) {
            rchar = 97+GetRand(26);
        }
        
        return rchar;
    }
    
    public static String generateRandompassword()
    {
        int passwordsize = 14;
        byte[] bPassword = new byte[passwordsize];
        int b = 0;
        
        for (b=0;b<passwordsize;b++) {
            bPassword[b] = (byte) genRandomprintablechar();
        }
                
        String strPassword = new String (bPassword);
        
        return strPassword;
    }
    
    public static String EncryptTextPFX (String strPassword, String strPlaintext)
    {
        String strEncrypted = "";
        
        if (strPlaintext.startsWith("#EC") == false) {
            strEncrypted = "#EC" + EncryptTextEx(strPassword, strPlaintext);
        } else {
            strEncrypted = strPlaintext;
        }
                        
        return strEncrypted;
    }
    
    public static String DecryptTextPFX (String strPassword, String strCiphertext)
    {
        if (strCiphertext.startsWith("#EC") == true) {
            
            String strFiltered = strCiphertext.substring(3);
            String strDecrypted = DecryptTextEx(strPassword, strFiltered);
            return strDecrypted;          
            
        } else {
            return strCiphertext;
        }
    }
} 
