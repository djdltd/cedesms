/*
 * ViewMembersScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.util.StringUtilities;


/**
 * 
 */
class ViewMembersScreen extends MainScreen implements ListFieldCallback {
    
        // Members ------------------------------------------------------------------
    private ListField _listField;   
    private Vector _contacts = new Vector();
    private String _groupname = "";
    TrustedContactsList _trustedContactList;
    
    private MenuItem _addMenuitem = new MenuItem ("Add Contact", 100, 10)
    {        
        public void run()
        {
            _trustedContactList = new TrustedContactsList("select");
            
            UiApplication.getUiApplication().pushModalScreen(_trustedContactList);
                        
             if (_trustedContactList.getLastaction() == "selectcontact") {
                //Dialog.alert("Send Trusted Contact Request!");
                
                SingleTrustedContact selContact = new SingleTrustedContact();
                selContact = _trustedContactList.getSelectedcontact();
                
                selContact.addMembership(_groupname);
                
                //_address.setText(selContact._mobile);
                //_password.setText(selContact._password);
                
                _trustedContactList.updateContact(selContact);    
                
                populateMembers();
            }
        }        
    };
    
    private MenuItem _removeMenuitem = new MenuItem ("Remove Contact", 100, 10)
    {        
        public void run()
        {
            _trustedContactList = new TrustedContactsList("select");
            
            int index = _listField.getSelectedIndex();                 
             
            if (index >= 0) {
                SingleTrustedContact selContact = new SingleTrustedContact();                
                selContact = (SingleTrustedContact) _contacts.elementAt(index);
                selContact.deleteMembership(_groupname);
                _trustedContactList.updateContact(selContact);
                
                populateMembers();
            }                                     
        }
    };
    
    ViewMembersScreen(String strGroupname) {    
                
        setTitle(new LabelField(strGroupname + " contacts" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
                
        _listField = new ListField();        
        _listField.setCallback(this);        
        add(_listField);              
        addMenuItem(_addMenuitem);
        addMenuItem(_removeMenuitem);
        
        
        // Make the groupname global
        _groupname = strGroupname;
        
        // Populate this groups members
        populateMembers();
        

    }
    
    public void populateMembers()
    {
        _contacts.removeAllElements();
        
        TrustedContactsList tcontacts = new TrustedContactsList("showall");
        
        int c = 0;
        SingleTrustedContact myContact = new SingleTrustedContact();
        
        for (c=0;c<tcontacts.getNumcontacts();c++) {
            myContact = tcontacts.getContactfromIndex(c);
            
            if (myContact.isGroupmember(_groupname) == true) {
                _contacts.addElement(myContact);
            }
        }
        
        // Refresh the list
        _listField.setSize(_contacts.size());
        
    }
    
    public void drawListRow(ListField listField, Graphics graphics, int index, int y, int width) 
    {
        if(listField == _listField && index < _contacts.size())
        {                
                SingleTrustedContact item = (SingleTrustedContact) _contacts.elementAt(index);
            
                
                String Fullname = "";
                if (item._first.length() > 0) {
                    Fullname+=item._first;
                }
                
                if (item._last.length() > 0) {
                    if (item._first.length() > 0) {
                        Fullname+=" ";                        
                    }
                    Fullname+=item._last;
                }                        
                                        
                graphics.drawText(Fullname, 0, y, 0, width);                
        }
    }

    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
     */
    public Object get(ListField listField, int index)
    {
        if(listField == _listField)
        {
            // If index is out of bounds an exception will be thrown, but that's the behaviour
            // we want in that case.
            return _contacts.elementAt(index);
        }
        
        return null;
    }
    
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
     */
    public int getPreferredWidth(ListField listField) 
    {
        // Use all the width of the current LCD.
        return getWidth();
    }
 
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField , String , int)
     */   
    public int indexOfList(ListField listField, String prefix, int start) 
    {
        return -1; // Not implemented.
    }    
    
    
} 
