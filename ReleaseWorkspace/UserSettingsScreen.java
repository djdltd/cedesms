/*
 * UserSettingsScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.LED;


/**
 * 
 */
class UserSettingsScreen extends MainScreen {
    
    private EditField _effirst;
    private EditField _eflast;
    private EditField _efred;
    private EditField _efgreen;
    private EditField _efblue;
    private EditField _efontime;
    private EditField _efofftime;
    private EditField _efsmsport;    
    
    private ObjectChoiceField _efchoice;
    private ObjectChoiceField _efplaysound;
    private ObjectChoiceField _efrepeatsound;
    private ObjectChoiceField _efencryptionmethod;
    private ObjectChoiceField _efshowdev;
    
    private UserSettings _settings = new UserSettings();
    private PersistentObject _persist;
    String[] strchoices = new String[8];
    String[] stryesno = new String[2];
    String[] strencryption = new String[2];
    
    private boolean _ledon = false;
    
    private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
    {
        public void run()
        {
            
            
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            
            setLocalsettings();
            _persist.commit();
            
            
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
            
        }
    };
 
    private MenuItem _ledMenuItem = new MenuItem("Toggle LED On/Off", 100, 10) 
    {
        public void run()
        {
                        
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            if (_ledon == false) {
                _ledon = true;
                FlashLED();
            } else {
                _ledon = false;
                StopLED();
            }
        }
    };
    
    UserSettingsScreen() {    
        setTitle(new LabelField("CedeSMS Options" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        //_fromaddress = new EditField("From:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);       
        _effirst = new EditField("Owner Firstname: ", "");
        _eflast = new EditField("Owner Lastname: ", "");
        
        
        strchoices[0] = "None";
        strchoices[1] = "1 min";
        strchoices[2] = "5 min";
        strchoices[3] = "10 min";
        strchoices[4] = "20 min";
        strchoices[5] = "30 min";
        strchoices[6] = "1 hour";
        strchoices[7] = "2 hours";
        
        stryesno[0] = "yes";
        stryesno[1] = "no";
        
        strencryption[0] = "CyberCede (Fast)";
        strencryption[1] = "CyberCede (Strong)";
        
        _efchoice = new ObjectChoiceField("Password prompt grace period: ", strchoices);
        
        _efred = new EditField("LED Color - Red Amount: ", "", 3, EditField.FILTER_INTEGER);
        _efgreen = new EditField("LED Color - Green Amount: ", "", 3, EditField.FILTER_INTEGER);
        _efblue = new EditField("LED Color - Blue Amount: ", "", 3, EditField.FILTER_INTEGER);
        _efontime = new EditField("LED Flash - On time (ms): ","",4, EditField.FILTER_INTEGER);
        _efofftime = new EditField("LED Flash - Off time (ms): ","",4, EditField.FILTER_INTEGER);
        
        _efsmsport = new EditField ("SMS Port: ", "", 4, EditField.FILTER_INTEGER);
        
        _efplaysound = new ObjectChoiceField("Play notification sound: ", stryesno);
        _efrepeatsound = new ObjectChoiceField("Repeat notification sound: ", stryesno);
                
        _efencryptionmethod = new ObjectChoiceField("Encryption method: ", strencryption);
        _efshowdev = new ObjectChoiceField("Show Developer options: ", stryesno);
        
        add(_effirst);
        add(_eflast);
        add(_efchoice);
        add(new SeparatorField());
        add(_efred);
        add(_efgreen);
        add(_efblue);
        add(_efontime);
        add(_efofftime);
        add(new SeparatorField());
        add(_efsmsport);
        add(new SeparatorField());
        add(_efplaysound);
        add(_efrepeatsound);
        
        add(new SeparatorField());
        
        if (GlobalAppProperties._islite == false) {
            add(_efencryptionmethod);
            add(new SeparatorField());
            add(_efshowdev);
        }
        
        addMenuItem(_saveMenuItem);
        addMenuItem(_ledMenuItem);
        
        loadSettings();
        setLocalform();
        
    }
    
    public boolean onSave()
    {
        //Dialog.alert("Auto Saved!");
        //setLocalcontact();
        //_recordsaved = true;        
        setLocalsettings();
        _persist.commit();
        return true;
    }
    
    public int getColorvalue(int red, int green, int blue)
    {                            
            byte [] intByte = new byte [4];
            
            intByte[0] = (byte) blue;
            intByte[1] = (byte) green;
            intByte[2] = (byte) red;
            intByte[3] = (byte) 0;
            
            int fromByte = 0;
            
            for (int i = 0; i < 4; i++)
            {
                int n = (intByte[i] < 0 ? (int)intByte[i] + 256 : (int)intByte[i]) << (8 * i);                
                fromByte += n;
            }
            
            return fromByte;        
    }
    
    public void FlashLED()
    {
        setLocalsettings();
        //LED.setColorConfiguration(500, 250, 0x00ffff00);
        LED.setColorConfiguration(_settings._ledontime, _settings._ledofftime, getColorvalue(_settings._ledredvalue, _settings._ledgreenvalue, _settings._ledbluevalue));
        
        
        //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
        LED.setState( LED.STATE_BLINKING );
    }
    
    public void StopLED()
    {
        LED.setState( LED.STATE_OFF );
    }
    
    public void loadSettings ()
    {
         // Hash of "CedeSMS".
         //9504d44735f128def178e9a3a10185f1
         long KEY =  0xef178e9a3a10185fL;
         _persist = PersistentStore.getPersistentObject( KEY );
         _settings = (UserSettings) _persist.getContents();
         if( _settings == null ) {
             _settings = new UserSettings();
             _persist.setContents( _settings );
             //persist.commit()
         }
    }
    
    int getSelcolour(String strValue)
    {
        int icolour = Integer.parseInt(strValue);
        if (icolour >= 0 && icolour <=255) {
            return icolour;
        } else {
            Dialog.alert("Colour values must be between 0 and 255.");
            return 0;
        }
    }
    
    int getSeltime(String strValue)
    {
        int itime = Integer.parseInt(strValue);
        if (itime >= 0 && itime <= 9000) {
            return itime;            
        } else {
            Dialog.alert("Time values must be between 0 and 9000.");
            return 250;
        }
    }
    
    public void setLocalsettings()
    {
        _settings._ownerfirstname = _effirst.getText();
        _settings._ownerlastname = _eflast.getText();
        _settings._smsport = _efsmsport.getText();
        _settings._passwordgrace = getGracechoice();        
        _settings._ledredvalue = getSelcolour(_efred.getText());
        _settings._ledgreenvalue = getSelcolour(_efgreen.getText());
        _settings._ledbluevalue = getSelcolour(_efblue.getText());
        _settings._ledontime = getSeltime(_efontime.getText());
        _settings._ledofftime = getSeltime(_efofftime.getText());
        _settings._playsound = getMultichoice(_efplaysound, stryesno);
        _settings._repeatsound = getMultichoice(_efrepeatsound, stryesno);
        _settings._encryptionmethod = getMultichoice (_efencryptionmethod, strencryption);
        _settings._showdevoptions = getMultichoice(_efshowdev, stryesno);
    }
    
    public void setLocalform()
    {
        _effirst.setText(_settings._ownerfirstname);
        _eflast.setText(_settings._ownerlastname);
        _efsmsport.setText (_settings._smsport);
        setGracechoice(_settings._passwordgrace);
        _efred.setText(String.valueOf(_settings._ledredvalue));
        _efgreen.setText(String.valueOf(_settings._ledgreenvalue));
        _efblue.setText(String.valueOf(_settings._ledbluevalue));
        _efontime.setText(String.valueOf(_settings._ledontime));
        _efofftime.setText(String.valueOf(_settings._ledofftime));
        setMultichoice(_efplaysound, _settings._playsound, stryesno);
        setMultichoice(_efrepeatsound, _settings._repeatsound, stryesno);
        setMultichoice(_efencryptionmethod, _settings._encryptionmethod, strencryption);
        setMultichoice(_efshowdev, _settings._showdevoptions,stryesno);
        
        //_efchoice.set
    }
    
    public void setGracechoice(String strChoice)
    {              
        if (strChoice.trim() == "") {
            _efchoice.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<strchoices.length;c++)
            {
                strcur = strchoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    _efchoice.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getGracechoice()
    {
        int index = _efchoice.getSelectedIndex();
        
        return strchoices[index];
    }
    
    public void setMultichoice(ObjectChoiceField myfield, String strChoice, String[] mychoices)
    {
        if (strChoice.trim() == "") {
            myfield.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<mychoices.length;c++)
            {
                strcur = mychoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    myfield.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getMultichoice (ObjectChoiceField myfield, String[] mychoices)
    {
        int index = myfield.getSelectedIndex();
        
        return mychoices[index];
    }
    
    
    public UserSettings getUserSettings()
    {
        return _settings;
    }
    
    public void setUserSettings (UserSettings settings)
    {
        _settings = settings;
        _persist.commit();
    }
    
    public void clearSettings ()
    {               
        _settings._initialsetupcompleted = "";
        _settings._ownerfirstname = "";
        _settings._ownerlastname = "";
        _settings._passwordgrace = "";
        _settings._encryptedtag = "";
        _settings._ledbluevalue = 0;
        _settings._ledgreenvalue = 255;
        _settings._ledredvalue = 0;
        _settings._ledontime = 500;
        _settings._ledofftime = 250;
        _settings._playsound = "yes";
        _settings._repeatsound = "yes";
        _settings._encryptionmethod = "CyberCede (Fast)";
        _settings._showdevoptions = "no";
        _settings._smsport = "3590";
        
        _persist.commit();
    }
}
 

