/*
 * ContactGroupsList.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.StringUtilities;

/**
 * 
 */
class ContactGroupsList  extends MainScreen implements ListFieldCallback {
    
    private ListField _listField;   
    private Vector _contactgroups = new Vector();
    private PersistentObject _persist;
    private AddContactGroupScreen _addcontactgroupscreen;
    private ViewMembersScreen _viewmembersscreen;
    private String _selectedGroup = "";
    private String _lastAction = "";
    UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
    
    private MenuItem _addMenuitem = new MenuItem ("Create Contact Group", 100, 10)
    {        
        public void run()
        {
            UserSettings mysettings = new UserSettings();
                
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();          
            
            if (mysettings._istrial.compareTo("yes") == 0) {
                if (_contactgroups.size() >=1) {
                    Dialog.alert("The Trial version of CedeSMS is restricted to 1 contact group. Please contact CedeSoft for purchase information.");
                    return;
                }
            }
            
            
            String groupname = "";
            _addcontactgroupscreen = new AddContactGroupScreen();                 
            UiApplication.getUiApplication().pushModalScreen(_addcontactgroupscreen);                        
            
            
            if (_addcontactgroupscreen.isSaved() == true) {
                groupname = _addcontactgroupscreen.getGroupname();
                
                _contactgroups.addElement(groupname);
                
                // Refresh the list
                _listField.setSize(_contactgroups.size());
                
                // save contact groups
                saveContactgroups();
            }
        }
    };
    
    private MenuItem _viewmembersMenuitem = new MenuItem ("View Members", 100, 10)
    {        
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            if (index >=0) {
            
                String strSelectedgroup = (String) _contactgroups.elementAt(index);
            
                _viewmembersscreen = new ViewMembersScreen(strSelectedgroup);
                UiApplication.getUiApplication().pushModalScreen(_viewmembersscreen);
            }           
        }        
    };
    
    private MenuItem _removeMenuitem = new MenuItem ("Remove Contact Group", 100, 10)
    {        
        public void run()
        {
            int index = _listField.getSelectedIndex();                       
            
            if (index >=0) {
            
                String strSelectedgroup = (String) _contactgroups.elementAt(index);
            
                if (Dialog.ask(Dialog.D_YES_NO, "Remove " + strSelectedgroup + " group?") == Dialog.YES) {
            
                    removeMemberships(strSelectedgroup);
                    
                    _contactgroups.removeElementAt(index);
                    
                    // Refresh the list
                    _listField.setSize(_contactgroups.size());            
                    
                    saveContactgroups();
                }
            }           
        }        
    };
    
    public void removeMemberships (String strGroupname)
    {
        TrustedContactsList trustedcontactlist = new TrustedContactsList("showall");
        
        int c = 0;
        SingleTrustedContact myContact = new SingleTrustedContact();
        
        for (c=0;c<trustedcontactlist.getNumcontacts();c++) {
            myContact = trustedcontactlist.getContactfromIndex(c);
            
            if (myContact.isGroupmember(strGroupname) == true) {
                myContact.deleteMembership(strGroupname);
            }
            
            trustedcontactlist.updateContact(myContact);
        }
    }
    
    private MenuItem _selectMenuItem = new MenuItem("Select Group", 100, 10) 
    {
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            if (_contactgroups != null) {
                if (_contactgroups.size() > 0) {
                    _selectedGroup = (String) _contactgroups.elementAt(index);
                }
            }
            
            //private SingleTrustedContact _selectedContact;
            //private String _lastAction;
            _lastAction = "selectgroup";
            
                        
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
            
        }
    };
        
    ContactGroupsList(String strMenuoptions) {
        
        setTitle(new LabelField("Contact Groups" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        _listField = new ListField();        
        _listField.setCallback(this);        
        add(_listField);
        
        // Add Menu Items                
        if (strMenuoptions.compareTo("showall") == 0) {
            addMenuItem(_viewmembersMenuitem);
            addMenuItem(_addMenuitem);
            addMenuItem(_removeMenuitem);
        }
        
        if (strMenuoptions.compareTo("selectonly") == 0) {
            addMenuItem(_selectMenuItem);
        }
        
        // Load the contact groups from persistent storage
        loadContactgroups();
        
        // Refresh the list
        _listField.setSize(_contactgroups.size());                
    }
     
    public void loadContactgroups()
    {
        //c5a61cccd8092eedb5dea9953434950b
         long KEY =  0xc5a61cccd8092eedL;
         _persist = PersistentStore.getPersistentObject( KEY );         
            _contactgroups = (Vector) _persist.getContents();
            if( _contactgroups == null ) {
                _contactgroups = new Vector();
                _persist.setContents( _contactgroups );
                //persist.commit()
            }
    }
        
    public void saveContactgroups()
    {
        _persist.commit();
    }
    
    public String getLastaction()
    {
        return _lastAction;
    }
    
    public String getSelectedgroup()
    {
        return _selectedGroup;
    }
    
    public void drawListRow(ListField listField, Graphics graphics, int index, int y, int width) 
    {
        if(listField == _listField && index < _contactgroups.size())
        {                
                String item = (String) _contactgroups.elementAt(index);
                                                                                   
                //graphics.drawText(Fullname, 0, y, 0, width);  
                graphics.drawText(item, 0, y, 0, width);               
        }
    }

    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
     */
    public Object get(ListField listField, int index)
    {
        if(listField == _listField)
        {
            // If index is out of bounds an exception will be thrown, but that's the behaviour
            // we want in that case.
            return _contactgroups.elementAt(index);
        }
        
        return null;
    }
    
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
     */
    public int getPreferredWidth(ListField listField) 
    {
        // Use all the width of the current LCD.
        return getWidth();
    }
 
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField , String , int)
     */   
    public int indexOfList(ListField listField, String prefix, int start) 
    {
        return -1; // Not implemented.
    }    
    
            
        private class AddContactGroupScreen extends MainScreen
        {
            private EditField _efname;
            private String _groupname;
            boolean _recordsaved = false;
            
            private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
            {
                public void run()
                {
        
                    _groupname = _efname.getText();                                        
                    _recordsaved = true;
                    
                    if (validateForm() == true) {
                        UiApplication uiapp = UiApplication.getUiApplication();
                        uiapp.popScreen(uiapp.getActiveScreen());
                    }                                   
                }
            };
            
            private MenuItem _cancelMenuItem = new MenuItem("Cancel", 100, 10)
            {
                public void run()
                {
                    _recordsaved = false;                                        
                    UiApplication uiapp = UiApplication.getUiApplication();
                    uiapp.popScreen(uiapp.getActiveScreen());                                                       
                }
            };
            
            // Constructor
            private AddContactGroupScreen()
            {
                setTitle(new LabelField("Add Contact Group", LabelField.USE_ALL_WIDTH));        
                
                _efname = new EditField("Group name: ", "");
                
                add(_efname);
                addMenuItem(_saveMenuItem);
                addMenuItem(_cancelMenuItem);
            }
            
            public boolean validateForm ()
            {
                if (_efname.getText().length() < 2) {
                    Dialog.alert("Please enter a valid Group name.");
                    return false;
                }                              
                return true;
            }
            
            public boolean isSaved()
            {
                return _recordsaved;
            }
            
            public String getGroupname()
            {
                return _groupname;
            }
            
            public boolean onSave()
            {
                _groupname = _efname.getText();               
                _recordsaved = true;
                
                return true;
            }
                            
            /**
            * Close application
            * 
            * @see net.rim.device.api.ui.Screen#close() 
            */
            public void close()
            {
                //_listener.stop();
                //_sender.stop();
                
                super.close();
                
            }
        }
} 
