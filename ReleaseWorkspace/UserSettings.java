/*
 * UserSettings.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import net.rim.device.api.util.Persistable;


/**
 * 
 */
class UserSettings implements Persistable {
    
    public String _ownerfirstname = "";
    public String _ownerlastname = "";
    public String _encryptedtag = "";
    public String _initialsetupcompleted = "";
    public String _passwordgrace = "";
    public int _ledredvalue = 0;
    public int _ledgreenvalue = 255;
    public int _ledbluevalue = 0;
    public int _ledontime = 500;
    public int _ledofftime = 250;
    public String _playsound = "yes";
    public String _repeatsound = "yes";
    public String _encryptionmethod = "CyberCede (Fast)";
    public String _showdevoptions = "no";
    public String _licenseentered = "";
    public String _licensekey = "";
    public String _istrial = "yes";
    public String _smsport = "3590";
    
    UserSettings() {    
    
    }
} 
