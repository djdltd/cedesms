/*
 * PasswordPopupScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;



import net.rim.device.api.i18n.*;
import net.rim.device.api.system.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
public class LicensePopupScreen extends PopupScreen implements KeyListener, TrackwheelListener {
   private String _response;
   // This EditField in RIM API provides built in password
   // masking so the password does not appear on the screen
   // as the user enters it
   private EditField answer;
   

    private String _strfirstname = "";
    private String _strlastname = "";
    private String _strpassword = "";
    private boolean _escaped = true;

   // Hard coded password, normally it is a better idea to have
   // the password stored in an internal RecordStore but here
   // it is hard coded for simplification
   private String password = "password";
   
   public LicensePopupScreen(boolean initialsetup)
   {
       super(new VerticalFieldManager(),Field.FOCUSABLE);
              
              
        LabelField question = new LabelField("License key:");
        answer = new EditField(" ","");
        add(question);
        add(new SeparatorField());
        add(answer);               
   }
   
   public String getFirstname()
   {
       return _strfirstname;
    }
    
    public String getLastname()
    {
        return _strlastname;
    }
    
    public String getPassword()
    {
        return _strpassword;
    }
    
    public boolean getEscaped()
    {
        return _escaped;
    }
   
   public boolean ValidateEntry ()
   {

        _strpassword = answer.getText();
        
        if (_strpassword.length() < 4) {
            Dialog.alert("Key too short.");
            return false;
        }
        
        _escaped = false;
        return true;
    }
   
   // This function gets called if the password gets called
   // it pops the password screen and pushes the apps main screen
   public void accept() {
       UiApplication.getUiApplication().popScreen(this);
   }
   public void close() {
       UiApplication.getUiApplication().popScreen(this);
   }
   public String getResponse() {
       return _response;
   }
   ////////////////////////////////////////////
   /// implementation of TrackwheelListener
   ////////////////////////////////////////////
   public boolean trackwheelClick(int status, int time) {
       _response = answer.getText();
       
       //if (_response.equals(password)) {
           //accept();
       //}
       //else {
           //Dialog.alert("Invalid Password !");
       //}
       
       if (ValidateEntry() == true) {
            accept();
        }
       
       return true;
   }
   /** Invoked when the trackwheel is released */
   public boolean trackwheelUnclick(int status, int time) {
       return false;
   }
   /** Invoked when the trackwheel is rolled. */
   public boolean trackwheelRoll(int amount, int status, int time) {
       return true;
   }
   /////////////////////////////////////
   /// implementation of Keylistener
   /////////////////////////////////////
   public boolean keyChar(char key, int status, int time) {
       //intercept the ESC key - exit the app on its receipt
       boolean retval = false;
       switch (key) {
           case Characters.ENTER:
               //_response = answer.getText();
               
               
               //if (_response.equals(password)) {
                   //accept();
               //}
               // an alert is displayed if the password is incorrect
               //else {
                   //Dialog.alert("Invalid Password !");
               //}
                if (ValidateEntry() == true) {
                    accept();
                }
               
               retval = true;
               break;
           case Characters.ESCAPE:
               _escaped = true;
               close();
               break;
           default:
               retval = super.keyChar(key,status,time);
       }
       return retval;
   }
   /** Implementation of KeyListener.keyDown */
   public boolean keyDown(int keycode, int time) {
       return false;
   }
   /** Implementation of KeyListener.keyRepeat */
   public boolean keyRepeat(int keycode, int time) {
       return false;
   }
   /** Implementation of KeyListener.keyStatus */
   public boolean keyStatus(int keycode, int time) {
       return false;
   }
   /** Implementation of KeyListener.keyUp */
   public boolean keyUp(int keycode, int time) {
       return false;
   }
}




