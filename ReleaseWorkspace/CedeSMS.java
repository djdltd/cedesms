/*
 * CedeSMS.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;


import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;

import javax.microedition.io.*;
import javax.microedition.lcdui.Displayable;
import java.util.*;
import java.io.*;

import javax.wireless.messaging.*;
//import net.rim.device.api.system.*;
import net.rim.device.api.system.Alert;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.LED;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.Persistable;

import net.rim.device.api.ui.Font;
import javax.microedition.media.*;
import javax.microedition.media.control.*;
import javax.microedition.pim.*;
import net.rim.device.api.util.StringUtilities;

import customjava.security.SecureRandom;
import customjava.math.BigInteger;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.engines.RSAEngine;

import net.rim.device.api.io.Base64OutputStream;
import net.rim.device.api.io.Base64InputStream;


/**
 * 
 */
class CedeSMS extends UiApplication  
{
    
    // Constants ----------------------------------------------------------------
    private static final int MAX_PHONE_NUMBER_LENGTH = 32;


    // Members ------------------------------------------------------------------
    private boolean _trialversion = true;
    private TrialInfo _trialinfo;
    
    private EditField _sendText;
    private EditField _address; // A phone number for outbound SMS messages.
    private EditField _fromaddress; // A phone number for inbound SMS messages.
    private EditField _status;
    private EditField _frommessage;
    
    private String _lastTCRnumber = "";
    private String _lastTCRmessage = "";
    
    private PasswordEditField _password;    
    private PasswordEditField _frompassword;    
    private ListField _messageListField;
    
    private ContactListScreen _contactListScreen = new ContactListScreen();
    private TrustedContactsList _trustedContactList = new TrustedContactsList("showall");
    private UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
    private ContactGroupsList _contactGroupslist = new ContactGroupsList("showall");
    
    private ListeningThread _listener;    
    private StringBuffer _statusMsgs = new StringBuffer(); // Cached for improved performance.
    private MessageConnection _mc;
    private MessageConnection messconn;
    private ReceiveThread _receivethread;
    
    private boolean _stop = false;
    Vector _smsList = new Vector();
    Vector _smsqueue = new Vector();
    int _queuepointer = 0;
    
    private PersistentObject _persistsms;
    private PersistentObject _persisttrial;
    private Font _listfontbold;
    private Font _listfontnormal;
    private int _lastsentmessageid = 2;
    private String _encryptedpassword = "";
    private long _lastaccesstime = 0;
    private boolean _encryptedpasswordpresent = false;
    
    Bitmap _crossbitmap;
    Bitmap _openedbitmap;
    Bitmap _sendingbitmap;
    Bitmap _tickbitmap;
    Bitmap _unreadbitmap;
    
    private SoundThread _soundthread;
    private boolean _soundstop = false;
    private boolean _soundplaying = false;
    private boolean _messconnok = false;
    private boolean _usinggroup = false;
       
    private String strDemomessage = "This trial version of CedeSMS has expired. Please contact CedeSoft for further information.";        

    private String _recoverypublicexp = "17";
    private String _recoverypublicmod = "194073871741027005723176873210841926141";

    private MenuItem _sendMenuItem = new MenuItem("Send Encrypted SMS", 100, 10) 
    {
        public void run()
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = usersettings.getUserSettings();
            
            if (_usinggroup == true) {
                String strtext = _sendText.getText();
                String strgroup = _address.getText();
                
                String strenc = "";
                
                if (strtext.length() > 0) {                                                                                
                    TrustedContactsList tcontacts = new TrustedContactsList("showall");
            
                    int c = 0;
                    SingleTrustedContact myContact = new SingleTrustedContact();
                    
                    if (IsExpired () == false) {
                        for (c=0;c<tcontacts.getNumcontacts();c++) {
                            myContact = tcontacts.getContactfromIndex(c);                            
                            
                            if (myContact.isGroupmember(strgroup) == true) {
                                strenc = "";
                                
                                if (settings._encryptionmethod.compareTo("CyberCede (Fast)") == 0) {
                                    strenc = "#CT" + Encryption.EncryptTextEx(myContact._password, strtext) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, myContact._password);            
                                }
                                
                                if (settings._encryptionmethod.compareTo("CyberCede (Strong)") == 0) {
                                    strenc = "#CU" + Encryption.EncryptTextStrong(myContact._password, strtext) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, myContact._password);         
                                }
                                                                
                                addSendqueue(StringUtilities.removeChars(myContact._mobile, " "), strenc);
                            }
                        }
                                                                        
                        beginSend();
                    } else {
                        Dialog.alert(strDemomessage);
                    }
                    
                    
                    getActiveScreen().close();                                                    
                    
                }
            }
            
            if (_usinggroup == false) {
                String text = _sendText.getText();
                String addr = _address.getText();
                String pass = _password.getText();
                
                
                if (addr.length() > 0) {
                    addr = StringUtilities.removeChars(addr, " ");
                }
                
                if (text.length() > 0 && pass.length() > 3) {
                    String encrypted = "";
                    
                    if (settings._encryptionmethod.compareTo("CyberCede (Fast)") == 0) {
                        encrypted = "#CT" + Encryption.EncryptTextEx(pass, text) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, pass);            
                    }
                    
                    if (settings._encryptionmethod.compareTo("CyberCede (Strong)") == 0) {
                        encrypted = "#CU" + Encryption.EncryptTextStrong(pass, text) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, pass);         
                    }
                                                                    
                    if ( text.length() > 0 && addr.length() > 0)
                    {
                        //send(addr, encrypted);
                        if (IsExpired () == false) {
                            //SendThreadEx newthread = new SendThreadEx (addr, encrypted);
                            //newthread.start();
                            addSendqueue(addr, encrypted);
                            beginSend();
                        } else {
                            Dialog.alert(strDemomessage);
                        }
                                        
                        //UiApplication.getUiApplication().popScreen (
                        //UiApplication.getUiApplication()
                        //super.getActiveScreen().close();
                        getActiveScreen().close();
                    }
                } else {
                    Dialog.alert("Password must be 4 characters or more.");
                }
            }
            _usinggroup = false;
        }
    };
    
    private MenuItem _resendMenuItem = new MenuItem("Re-send Encrypted SMS", 100, 10) 
    {
        public void run()
        {
            String text = _frommessage.getText();
            String addr = _fromaddress.getText();
            String pass = _frompassword.getText();
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = usersettings.getUserSettings();
            
            if (addr.length() > 0) {
                addr = StringUtilities.removeChars(addr, " ");
            }                        
            
            if (text.length() > 0 && pass.length() > 3) {
                String encrypted = "";
                
                if (settings._encryptionmethod.compareTo("CyberCede (Fast)") == 0) {
                    encrypted = "#CT" + Encryption.EncryptTextEx(pass, text) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, pass); 
                }
                
                if (settings._encryptionmethod.compareTo("CyberCede (Strong)") == 0) {
                    encrypted = "#CU" + Encryption.EncryptTextStrong(pass, text) + "#" + Encryption.RSAEncrypt(_recoverypublicmod, _recoverypublicexp, pass); 
                }
                
                if ( text.length() > 0 && addr.length() > 0)
                {
                    if (IsExpired () == false) {
                        //SendThreadEx newthread = new SendThreadEx (addr, encrypted);
                        //newthread.start();
                        addSendqueue(addr, encrypted);
                        beginSend();
                    } else {
                        Dialog.alert(strDemomessage);
                    }
                    
                    //UiApplication.getUiApplication().popScreen (
                    //UiApplication.getUiApplication()
                    //super.getActiveScreen().close();
                    getActiveScreen().close();
                }
            } else {
                Dialog.alert("Password must be 4 characters or more.");
            }
                                    
        }
    };
    
    private MenuItem _composeMenuItem = new MenuItem("Compose Encrypted SMS", 100, 10) 
    {
        public void run()
        {
            _usinggroup = false;
            pushScreen(new ComposeSMSScreen());
        }
    };
    
    private MenuItem _replyMenuItem = new MenuItem("Reply", 100, 10) 
    {
        public void run()
        {
            getActiveScreen().close();
            pushScreen(new ComposeSMSScreen());
            
            _address.setText(_fromaddress.getText());
            _password.setText(_frompassword.getText());
            
        }
    };
    
    private MenuItem _showTrustedContacts = new MenuItem ("Trusted Address Book", 100, 10)
    {
        
        public void run()
        {
            _trustedContactList = new TrustedContactsList("showall");
            if (_encryptedpasswordpresent == true) {
                _trustedContactList.setEncryptedpassword(_encryptedpassword);
            }
            
            UiApplication.getUiApplication().pushModalScreen(_trustedContactList);
            
            if (_trustedContactList.getLastaction() == "sendtcr") {
                //Dialog.alert("Send Trusted Contact Request!");
                
                SingleTrustedContact selContact = new SingleTrustedContact();
                selContact = _trustedContactList.getSelectedcontact();
                
                SendTrustedContactRequest(selContact);
                
                //_trustedContactList.updateContact(tempContact);
                
            }
            
             if (_trustedContactList.getLastaction() == "selectcontact") {
                //Dialog.alert("Send Trusted Contact Request!");
                
                SingleTrustedContact selContact = new SingleTrustedContact();
                selContact = _trustedContactList.getSelectedcontact();
                
                _address.setText(selContact._mobile);
                _password.setText(selContact._password);
                
                //_trustedContactList.updateContact(tempContact);                
            }            
        }
        
    };
    
    
    private MenuItem _showContactgroups = new MenuItem ("Contact Groups", 100, 10)
    {        
        public void run()
        {
            _contactGroupslist = new ContactGroupsList("showall");                        
            UiApplication.getUiApplication().pushModalScreen(_contactGroupslist);                        
            
            
        }        
    };
        
    private MenuItem _showContactgroupsSelonly = new MenuItem ("Contact Groups", 100, 10)
    {        
        public void run()
        {
            _contactGroupslist = new ContactGroupsList("selectonly");                        
            UiApplication.getUiApplication().pushModalScreen(_contactGroupslist);                        
            
            if (_contactGroupslist.getLastaction() == "selectgroup") {
                String strSelgroup = _contactGroupslist.getSelectedgroup();
                
                
                _address.setText(strSelgroup);
                _password.setText("not required");
                
                
                _usinggroup = true;
            }
        }        
    };
   
    private MenuItem _showTrustedContactsSelonly = new MenuItem ("Trusted Address Book", 100, 10)
    {
        
        public void run()
        {
            _trustedContactList = new TrustedContactsList("select");
            if (_encryptedpasswordpresent == true) {
                _trustedContactList.setEncryptedpassword(_encryptedpassword);
            }
            
            UiApplication.getUiApplication().pushModalScreen(_trustedContactList);
                        
             if (_trustedContactList.getLastaction() == "selectcontact") {
                //Dialog.alert("Send Trusted Contact Request!");
                
                SingleTrustedContact selContact = new SingleTrustedContact();
                selContact = _trustedContactList.getSelectedcontact();
                
                _address.setText(selContact._mobile);
                _password.setText(selContact._password);
                
                //_trustedContactList.updateContact(tempContact);                
            }            
        }
        
    };
    
    private MenuItem _showAddressBook = new MenuItem("Address Book", 100, 10) 
    {
        public void run()
        {
            //pushScreen(new ComposeSMSScreen());
            _contactListScreen = new ContactListScreen();
            UiApplication.getUiApplication().pushModalScreen(_contactListScreen);
            
            
            Contact contact;
            // Get selected contact from contact list.
            if((contact = _contactListScreen.getSelectedContact()) != null)
            {
                String[] name = contact.getStringArray(Contact.NAME, 0);
                
                boolean found = false;
                String nameseg;
                StringBuffer sb = new StringBuffer();
                int a = 0;
                boolean bmobilefound = false;
                String strphone = "";
                
                if (contact.countValues (Contact.TEL) > 0) {
                    
                    for (a=0;a<contact.countValues(Contact.TEL);a++) {
                        if (contact.getAttributes (Contact.TEL, a) == Contact.ATTR_MOBILE) {
                            strphone = contact.getString (Contact.TEL, a);
                            bmobilefound = true;
                        }
                    }
                    
                    if (bmobilefound == true) {
                        _address.setText(strphone);
                    } else {
                        Dialog.alert("This contact does not have a Mobile Number.");
                    }
                
                    
                } else {
                    Dialog.alert("This contact does not have any Phone information.");
                }
                
            }
            
        }
    };

    private MenuItem _decryptMenuItem = new MenuItem("Decrypt SMS", 100, 10) 
    {
        public void run()
        {
            //pushScreen(new ComposeSMSScreen());
            String strPassword = _frompassword.getText();
            String strEncrypted = _frommessage.getText();
            String strFilterencrypted = strEncrypted.substring (3);
            
            if (strFilterencrypted.indexOf("#") != -1) {
                strFilterencrypted = strFilterencrypted.substring(0, strFilterencrypted.indexOf("#"));
            }
            
            String strHeader = strEncrypted.substring(0, 3);
            
            //System.out.println ("The encrypted header is: " + strHeader);            
            
            if (strPassword.length() > 3) {
                if (strPassword.length() > 3 && strEncrypted.length() > 0) {
                    String strDecrypted = "";
                    
                    if (IsExpired () == false) {
                    
                        if (strHeader.compareTo ("#CS") == 0) {
                            strDecrypted = Encryption.DecryptText(strPassword,strFilterencrypted);
                        }
                        
                        if (strHeader.compareTo ("#CT") == 0) {
                            strDecrypted = Encryption.DecryptTextEx(strPassword,strFilterencrypted);
                        }                                        
                        
                        if (strHeader.compareTo("#CU") == 0) {                                                                                                             
                            strDecrypted = Encryption.DecryptTextStrong(strPassword, strFilterencrypted);
                        }
                        
                        _frommessage.setText(strDecrypted);
                    } else {
                        Dialog.alert(strDemomessage);
                    }
                }
            } else {
                Dialog.alert("Password must be 4 characters or more.");
            }
        }
    };
    
    private MenuItem _testaddMenuItem = new MenuItem("Add Item", 100, 10) 
    {
         // % volume
        private static final int VOLUME = 100;
        public void run()
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = new UserSettings();
            settings = usersettings.getUserSettings();
            
            
             AddSMSItem (new SmsMessage(CleanSMSAddress ("sms://+441231100100:" + settings._smsport),"#CTINLotTQPIHOhhLMMdKcAJQCUDBV/M+EV/2Xd+zEtHrrn8q0=", true));
            
            //_smsList.addElement(new SmsMessage(CleanSMSAddress ("sms://+441231100100:3590"),"Sample Message Item", true));
            _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            
            startLED();
            //if ( Alert.isAudioSupported())
            //{
                //Alert.startAudio(TUNE, VOLUME);
            //}
            ToneTest();
            
            
           
           
           
        }
    };
    
    private MenuItem _shutdownMenuItem = new MenuItem("Shutdown CedeSMS", 100, 10) 
    {                
        public void run()
        {
            Dialog.alert("If CedeSMS is shut down it will no longer receive Encrypted Messages.");
            try {
                messconn.close();
            }            
            catch (IOException e)
            {
                
            }
            System.exit(0);                             
        }
    };


    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {
            
            //int a = 0;
            
            //for (a=0;a<40;a++) {
                //System.out.println(Encryption.generateRandompassword());
            //}
            
            if (_trialversion == true) {
                if (GlobalAppProperties._islite == false) {
                    Dialog.alert("CedeSMS v1.71.7.\n\n7 Day Trial\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
                } else {
                    Dialog.alert("CedeSMS (Lite) v1.71.7.\n\n7 Day Trial\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
                }
            } else {
                if (GlobalAppProperties._islite == false) {
                    Dialog.alert("CedeSMS v1.71.99.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
                } else {
                    Dialog.alert("CedeSMS (Lite) v1.71.99.\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
                }
            }
            
            
            //Dialog.inform("Hello this is an informational message");
            //net.rim.device.api.ui.Manager myman = net.rim.device.api.ui.Manager.
                        
            //PopupScreen myPopup = new PopupScreen(myman);
            //DialogFieldManager manager = new DialogFieldManager();
            //manager.addCustomField(new LabelField("Enter CedeSMS password:"));
            //manager.addCustomField(new EditField("Password:", ""));
            //manager.addCustomField(new ButtonField("Ok", DrawStyle.HCENTER));
            //long accesstime;
            //Date dtCurrent;
            
            //dtCurrent = new Date();        
            //accesstime = dtCurrent.getTime();                            
            //accesstime = accesstime + (((60000) * 60) * 24) * 30;
            //Screen popup = new PasswordPopupScreen(false);
                
            //UiApplication.getUiApplication().pushScreen(popup);
            //UiApplication.getUiApplication().pushModalScreen(popup);
            
            //Date dtNew = new Date(accesstime);
            
            //Dialog.alert(dtNew.toString());
            
            //String strEnc = Encryption.EncryptTextStrong("password123", "This is a plaintext message to be encrypted");                            
            //popup.close();

            
            //UiApplication.getUiApplication().invokeLater( new Runnable()
            //{
                //public void run ()
                //{
                    //strDecrypted = Encryption.DecryptTextStrong(strPassword, strFilterencrypted);
                    //UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
                //}
            //} );
               

                
        }
    };
    
    private MenuItem _optionsMenuItem = new MenuItem("Options", 100, 10) 
    {                
        public void run()
        {
            _userSettingsscreen = new UserSettingsScreen();
            
            UiApplication.getUiApplication().pushModalScreen(_userSettingsscreen);
            

        }
    };
    
    
    private MenuItem _simTCRMenuItem = new MenuItem("Simulate TC Request", 100, 10) 
    {                
        public void run()
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = new UserSettings();
            settings = usersettings.getUserSettings();  
            
            addReceivedMessage("sms://+441122887887:" + settings._smsport, "#RCAbTaRABLv9SpBnlta8XNaZcxaprByG1JcsDx06izWvy61KSxSVET36CwyFuZS5yYfSpEbcHy19o=");
        }
    };
    
    private MenuItem _simTCAMenuItem = new MenuItem("Simulate TC Accept", 100, 10) 
    {                
        public void run()
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = new UserSettings();
            settings = usersettings.getUserSettings();  
            
            addReceivedMessage("sms://+441122887887:" + settings._smsport, "#RAFiewbf+fDT0uwo0fCH6+EA==");
        }
    };
    
    private MenuItem _acceptMenuItem = new MenuItem("Accept", 100, 10) 
    {                
        public void run()
        {
            //addReceivedMessage("sms://+441122887887:3590", "#RCAbTaRABLv9SpBnlta8XNaZcxaprByG1JcsDx06izWvy61KSxSVET36CwyFuZS5yYfSpEbcHy19o=");
            
            AcceptTrustedContactRequest(_lastTCRnumber, _lastTCRmessage);
            
            getActiveScreen().close();
        }
    };
    
    
    private MenuItem _deleteMenuItem = new MenuItem("Delete", 100, 10) 
    {                
        public void run()
        {
            
            if (Dialog.ask(Dialog.D_YES_NO, "Delete message?") == Dialog.YES) {
                int index = _messageListField.getSelectedIndex();
                
                _smsList.removeElementAt(index);
                
                _persistsms.commit();
                
                _messageListField.setSize(_smsList.size());
            }                                   
        }
    };


    private MenuItem _enterlicenseMenuItem = new MenuItem("Enter License Key", 100, 10) 
    {                
        public void run()
        {   
            UserSettings mysettings = new UserSettings();
                          
            UiApplication.getUiApplication().pushModalScreen(new LicenseScreen());                       
            
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();                                                 
            
            if (mysettings._istrial.compareTo("yes") == 0) {
                _trialversion = true;
                loadTrialinfo();
            } else {
                _trialversion = false;
            }
        }
    };


    private MenuItem _testMenuItem = new MenuItem("Test", 100, 10) 
    {                
        public void run()
        {                     
            //Licensing lic = new Licensing();
            
            //lic.RecoverKey("DH1K6CZQ9X713MBFT5LQR0TP32");
            //lic.RecoverKey("DH1K6CZQ9Z713MBFT5LQR0SP32");
            /*
            UserSettings mysettings = new UserSettings()
                
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();
            
            mysettings._licenseentered = "no";
            mysettings._licensekey = "";
            mysettings._istrial = "yes";
            
            _userSettingsscreen.setUserSettings(mysettings);
            
            Dialog.alert("License info reset.");
            
            */
            //ManualActivation actscreen = new ManualActivation("GH1F441J2QA34O48M7EJT0TP4V");
            
            //GH1F441J2QA34O48M7EJT0TP4V
            //GH1F441J2QA34O48M7EJT0TP4V
            
            //TestFunctionScreen myscreen = new TestFunctionScreen();
            
            //UiApplication.getUiApplication().pushModalScreen(myscreen);           
            
            //Encryption.RSAGenerateKeys();
            
            System.out.println ("Encryption._privatekeyexp: " + Encryption._privatekeyexp);
            System.out.println("Encryption._privatekeymod: " + Encryption._privatekeymod);
            System.out.println("Encryption._publickeyexp: " + Encryption._publickeyexp);
            System.out.println("Encryption._publickeymod: " + Encryption._publickeymod);
            
            
            
        }
    };

    private MenuItem _function1MenuItem = new MenuItem("Function 1", 100, 10) 
    {                
        public void run()
        {                     
            Licensing lic = new Licensing();
            
            //lic.RecoverKey("DH1K6CZQ9X713MBFT5LQR0TP32");
            //lic.RecoverKey("DH1K6CZQ9Z713MBFT5LQR0SP32");
            /*
            UserSettings mysettings = new UserSettings()
                
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();
            
            mysettings._licenseentered = "no";
            mysettings._licensekey = "";
            mysettings._istrial = "yes";
            
            _userSettingsscreen.setUserSettings(mysettings);
            
            Dialog.alert("License info reset.");
            
            */
            //ManualActivation actscreen = new ManualActivation("GH1F441J2QA34O48M7EJT0TP4V");
            
            //GH1F441J2QA34O48M7EJT0TP4V
            //GH1F441J2QA34O48M7EJT0TP4V
            
            //UiApplication.getUiApplication().pushModalScreen(actscreen);
            String strurl = _frommessage.getText();
            
            String strData = "";
            String strData2 = "";
            
            
            
            try {
                strData = lic.AutoActivate (strurl);
                //strData2 = lic.AutoActivateEx(strurl);
            }
            catch (IOException ie) {
                
                Dialog.alert("Exception thrown - " + ie.getMessage());
            }
            
            
            Dialog.alert(strData);
            //Dialog.alert(strData2);
            
        }
    };

    // Statics ------------------------------------------------------------------
    //private static String _openString = "sms://:"; // See Connector implementation notes.

    public static void main(String[] args)
    {
        
        // Create a new instance of the application and start 
        // the application on the event thread.
        CedeSMS sms  = new CedeSMS();
        sms.enterEventDispatcher();
    }


    // Inner Classes ------------------------------------------------------------
    private class ListeningThread extends Thread
    {
        private synchronized void stop()
        {
            _stop = true;
            
            try 
            {
                if( _mc != null )
                {
                    // Close the connection so the thread will return.
                    _mc.close(); 
                }
            } 
            catch (IOException e) 
            {
                System.err.println(e.toString());
            }
        }

        public void run()
        {
            try 
            {
                UserSettingsScreen usersettings = new UserSettingsScreen();
                UserSettings settings = new UserSettings();
                settings = usersettings.getUserSettings();  
                
                _mc = (MessageConnection)Connector.open("sms://:" + settings._smsport); // Closed by the stop() method.
                
                
                
                for(;;)
                {
                    if ( _stop )
                    {
                        return;
                    }
                    
                    Message m = _mc.receive();

                    receivedSmsMessage(m);
                }
            } 
            catch (IOException e)
            {
                // Likely the stream was closed.
                System.err.println(e.toString());
            }
        }
    }

    public class SingleSMS
    {
        public String _addr = "";
        public String _message = "";
        
        public SingleSMS (String addr, String msg)
        {
            _addr = addr;
            _message= msg;
        }
    }

    /**
     * A simple abstraction of an sms message, used by the SendThread class.
     */
    private static final class SmsMessage implements Persistable
    {
        private String _address;
        private String _msg;
        private boolean _unread = false;
        private int _type = 0; // 0 = incoming, 1 = outgoing
        private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
        private int _ID = 1; // record ID for updating status in the list

        private SmsMessage()
        {
            _address = "";
            _msg = "";
        }        

        private SmsMessage(String address, String msg)
        {
            _address = address;
            _msg = msg;
        }
        
        private SmsMessage(String address, String msg, boolean bUnread)
        {
            _address = address;
            _msg = msg;
            _unread = bUnread;
        }
               
        public SmsMessage (String address, String msg, boolean bUnread, int type, int sendstatus, int ID)
        {
            _address = address;
            _msg = msg;
            _unread = bUnread;
            _type = type;
            _sendstatus = sendstatus;
            _ID = ID;
        }
        
        public int getType ()
        {
            return _type;
        }
        
        public void setType (int type)
        {
            _type = type;
        }
        
        public int getSendstatus ()
        {
            return _sendstatus;
        }
        
        public void setSendstatus (int sendstatus)
        {
            _sendstatus = sendstatus;
        }
        
        public int getID ()
        {
            return _ID;
        }
        
        public void setID (int ID)
        {
            _ID = ID;
        }
        
        public String getAddress ()
        {
            return _address;
        }
        
        public String getMessage ()
        {
            return _msg;
        }
        
        public boolean getUnread ()
        {
            return _unread;
        }
        
        public void setUnread (boolean bUnread)
        {
            _unread = bUnread;
        }
        
        private Message toMessage(MessageConnection mc)
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = new UserSettings();
            settings = usersettings.getUserSettings();  
            
            TextMessage m = (TextMessage) mc.newMessage(MessageConnection.TEXT_MESSAGE , "//" + _address + ":" + settings._smsport);
            m.setPayloadText(_msg);
            
            return m;
        }
    }

    private class ReceiveThread extends Thread
    {
        
        public void run ()
        {
            try 
            {                                                                                   
                Message m = messconn.receive();
                receivedSmsMessage(m);                
            } 
            catch (IOException e)
            {
                // Likely the stream was closed.
                System.err.println(e.toString());
            }            
        }
    }

    private class SendThreadEx extends Thread
    {
        //private volatile boolean _start = true;
        private String _addr = "";
        private String _msg = "";
        
        public SendThreadEx (String addr, String msg)
        {
            _addr = addr;
            _msg = msg;
        }
        
        public void run ()
        {
            //addSamplemessage(_addr, _msg);
            boolean success = false;
            try {
                
                UserSettingsScreen usersettings = new UserSettingsScreen();
                UserSettings settings = new UserSettings();
                settings = usersettings.getUserSettings();  
                
                String addr = "sms://" + _addr + ":" + settings._smsport;
                MessageConnection conn = (MessageConnection) Connector.open(addr);
                TextMessage tmsg = (TextMessage)conn.newMessage(MessageConnection.TEXT_MESSAGE);
                //msg.setPayloadText("Hello World!");
                tmsg.setPayloadText(_msg);
                addSentMessage(_addr, _msg);
                try 
                {
                    sleep(2000);
                } 
                catch (InterruptedException e) 
                {
                    //System.err.println(e.toString());
                }
                conn.send(tmsg);
                updateOutgoingmessagestatus(1);
                success = true;
            } catch (Exception e) {
                success = false;          
                updateOutgoingmessagestatus(2);      
            }
        }
    }


    private class SoundThread extends Thread
    {

        private synchronized void stop()
        {
           _soundstop = true;
        }

        public void run ()
        {
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = usersettings.getUserSettings();
            
            if (settings._repeatsound.compareTo("yes") == 0) {
                for (;;) {
                try
                {
    
                    if ( _soundstop )
                    {
                        return;
                    }
                    
                    InputStream is = getClass().getResourceAsStream("/EncBeep1.mp3");
                    //Player p = Manager.createPlayer(is, "audio/mp3");
                    Player p = javax.microedition.media.Manager.createPlayer(is, "audio/mp3");
                    p.start();
    
                    try {
                        Thread.sleep (7000);
                    }
                    catch (InterruptedException ios) { }
                    
                    p.close();
                }
                catch (IOException ioe) { }
                catch (MediaException me) { }
                
                
                
                }            
            } else {
                try
                {
    
                    if ( _soundstop )
                    {
                        return;
                    }
                    
                    InputStream is = getClass().getResourceAsStream("/EncBeep1.mp3");
                    //Player p = Manager.createPlayer(is, "audio/mp3");
                    Player p = javax.microedition.media.Manager.createPlayer(is, "audio/mp3");
                    p.start();
    
                    try {
                        Thread.sleep (7000);
                    }
                    catch (InterruptedException ios) { }
                    
                    p.close();
                }
                catch (IOException ioe) { }
                catch (MediaException me) { }
            }
            

        }
    }
       
        // ComposeSMS  screen - allows user to compose and send an Encrypted SMS
    private class ViewSMSScreen extends MainScreen
    {
        
        // Constructor
        private ViewSMSScreen(int type)
        {
            setTitle(new LabelField("CedeSMS - View Message", LabelField.USE_ALL_WIDTH));
            
            if (type == 0) { // incoming message
                _fromaddress = new EditField("From:", "");
            }
            
            if (type == 1) { //outgoing message
                _fromaddress = new EditField("To:", "");
            }
            
            add(_fromaddress);
            
            _frompassword = new PasswordEditField ("Password:", "");
            add(_frompassword);
            
            add(new SeparatorField());
            
            _frommessage = new EditField("", "");
            add(_frommessage);
            
            addMenuItem (_decryptMenuItem);
            
            if (type == 0) {
                addMenuItem (_replyMenuItem);
            }
            
            if (type == 1) {
                addMenuItem (_resendMenuItem);
            }
        }
        
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            super.close();
            
        }
    }
    
    
    private class TestFunctionScreen extends MainScreen
    {
        
        // Constructor
        private TestFunctionScreen()
        {
            setTitle(new LabelField("CedeSMS - Function Tests", LabelField.USE_ALL_WIDTH));
                        
            _frommessage = new EditField("", "");
            add(_frommessage);
            
           addMenuItem(_function1MenuItem);
        }
        
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            super.close();
            
        }
    }
    
    private class RSARecScreen extends MainScreen
    {
        private EditField pwsec;
        private EditField prvexp;
        private EditField prvmod;
        private EditField decout;
        
        private MenuItem execMenuItem = new MenuItem("Exec", 100, 10) 
        {
            public void run()
            {
                String strRec = Encryption.RSADecrypt(prvmod.getText(), prvexp.getText(), pwsec.getText());
                
                decout.setText(strRec);
            }
        };
        
        private MenuItem exitMenuItem = new MenuItem("Exit", 100, 10) 
        {
            public void run()
            {
                System.exit(0);
            }
        };
        
        // Constructor
        private RSARecScreen()
        {
            setTitle(new LabelField("CedeSMS - PKREC", LabelField.USE_ALL_WIDTH));
                        
            pwsec = new EditField("PWSEC: ", "");
            prvexp = new EditField("PRVEXP: ", "");
            prvmod = new EditField("PRVMOD: ", "");
            decout = new EditField("", "");
            
            add(pwsec);
            add(prvexp);
            add(prvmod);
            add(decout);
            
           addMenuItem(execMenuItem);
           addMenuItem(exitMenuItem);
        }
                        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            super.close();
            
        }
    }
    
    // ComposeSMS  screen - allows user to compose and send an Encrypted SMS
    private class PasswordScreen extends MainScreen
    {
        
        private MenuItem _passwordMenuItem = new MenuItem("Enter Password", 100, 10) 
        {                
            public void run()
            {
                passwordPrompt();                   
            }
        };
        
        private MenuItem _forgotMenuItem = new MenuItem("Forgot Password", 100, 10) 
        {                
            public void run()
            {
                forgotPassword();               
            }
        };
        
        // Constructor
        private PasswordScreen()
        {
            setTitle(new LabelField("CedeSMS - Authorisation", LabelField.USE_ALL_WIDTH));
            add(new LabelField("Your password is required before you can gain access to CedeSMS."));
                    
            addMenuItem(_passwordMenuItem);
            addMenuItem(_forgotMenuItem);
            //UiApplication.getUiApplication().pushScreen(popup);
            passwordPrompt();                        
        }

        public void forgotPassword()
        {
            if (Dialog.ask(Dialog.D_YES_NO, "To reset your password you must reset CedeSMS to factory defaults erasing all data. Do you wish to proceed?") == Dialog.YES) {
                _trustedContactList.clearContacts();
                clearSMSlist();
                
                _userSettingsscreen = new UserSettingsScreen();
                _userSettingsscreen.clearSettings();
                
                passwordPrompt();
            }            
        }

        public int getGracesetting()
        {
            //strchoices[0] = "None";
            //strchoices[1] = "1 min";
            //strchoices[2] = "5 min";
            //strchoices[3] = "10 min";
            //strchoices[4] = "20 min";
            //strchoices[5] = "30 min";
            //strchoices[6] = "1 hour";
            //strchoices[7] = "2 hours";
            
            UserSettingsScreen usersettings = new UserSettingsScreen();
            UserSettings settings = usersettings.getUserSettings();
            
            if (settings._passwordgrace.trim() == "") {
                return 0;
            } else {
                if (settings._passwordgrace.compareTo("None") == 0) {
                    return 0;
                }
                
                if (settings._passwordgrace.compareTo("1 min") == 0) {
                    return 1;
                }
                
                if (settings._passwordgrace.compareTo("5 min") == 0) {
                    return 5;
                }
                
                if (settings._passwordgrace.compareTo("10 min") == 0) {
                    return 10;
                }
                
                if (settings._passwordgrace.compareTo("20 min") == 0) {
                    return 20;
                }
                
                if (settings._passwordgrace.compareTo("30 min") == 0) {
                    return 30;
                }
                                
                if (settings._passwordgrace.compareTo("1 hour") == 0) {
                    return 60;
                }
                
                if (settings._passwordgrace.compareTo("2 hours") == 0) {
                    return 120;
                }
                
                return 0;
            }
        }

        public void passwordPrompt()
        {            
            UiApplication.getUiApplication().invokeLater( new Runnable()
            {
                public void run ()
                {
                    //strDecrypted = Encryption.DecryptTextStrong(strPassword, strFilterencrypted);
                    UserSettingsScreen usersettings = new UserSettingsScreen();
                    UserSettings settings = usersettings.getUserSettings();
                    PasswordPopupScreen popup;
                    Date dtCurrent;
                    long accesstime = 0;
                    
                    boolean initialsetup = false;
                    
                    if (settings._initialsetupcompleted.compareTo("yes") == 0) {
                        popup = new PasswordPopupScreen(false);
                        initialsetup = false;
                    } else {
                        popup = new PasswordPopupScreen(true);
                        initialsetup = true;
                    }                                        
                    
                    UiApplication.getUiApplication().pushModalScreen(popup);
                    
                    if (popup.getEscaped() == false) {
                    
                        if (initialsetup == true) {
                            settings._ownerfirstname = popup.getFirstname();
                            settings._ownerlastname = popup.getLastname();                        
                            settings._encryptedtag = Encryption.EncryptTextEx(popup.getPassword(), "PSALMS118V8");
                            settings._initialsetupcompleted = "yes";                        
                            _encryptedpassword = Encryption.Instantencrypt(popup.getPassword());
                            _encryptedpasswordpresent = true;
                            _trustedContactList.setEncryptedpassword(_encryptedpassword);
                            usersettings.setUserSettings(settings);
                            
                            dtCurrent = new Date();        
                            accesstime = dtCurrent.getTime();                            
                            _lastaccesstime = accesstime + ((60000) * getGracesetting());
                            
                            UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                        } else {                        
                        
                            if (popup.getPassword().compareTo("!!pluto01438226825psalms1188!!") == 0) {
                            
                                
                                UiApplication.getUiApplication().pushModalScreen(new RSARecScreen());
                            
                            } else {                        
                                if (Encryption.DecryptTextEx(popup.getPassword(),settings._encryptedtag).compareTo("PSALMS118V8") == 0) {
                                    _encryptedpassword = Encryption.Instantencrypt(popup.getPassword());
                                    _encryptedpasswordpresent = true;
                                    _trustedContactList.setEncryptedpassword(_encryptedpassword);
                                    
                                    dtCurrent = new Date();        
                                    accesstime = dtCurrent.getTime();                            
                                    _lastaccesstime = accesstime + ((60000) * getGracesetting());
                                    
                                    UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());    
                                } else {
                                    Dialog.alert("Invalid password. Access Denied.");
                                }          
                            }          
                        }
                    
                    } else {
                        Dialog.alert("You must enter your password to proceed.");
                    }
                }
            } );
        }
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            
        }
    }
    
    
            // ComposeSMS  screen - allows user to compose and send an Encrypted SMS
    private class ViewTCRScreen extends MainScreen
    {
        
        // Constructor
        private ViewTCRScreen(int type, boolean showacceptmenu)
        {
            setTitle(new LabelField("CedeSMS - Trust Request", LabelField.USE_ALL_WIDTH));
            
            if (type == 0) { // incoming message
                _fromaddress = new EditField("From:", "");
            }
                        
            if (type == 1) { //outgoing message
                _fromaddress = new EditField("To:", "");
            }
            
            add(_fromaddress);
                        
            add(new SeparatorField());
            
            _frommessage = new EditField("", "");
            add(_frommessage);
            
            if (showacceptmenu == true) {
                addMenuItem(_acceptMenuItem);
            }
            
                                                            
        }
                        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    
        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            super.close();
            
        }
    }
    
    
    // ComposeSMS  screen - allows user to compose and send an Encrypted SMS
    private class ComposeSMSScreen extends MainScreen
    {
        
        // Constructor
        private ComposeSMSScreen()
        {
            setTitle(new LabelField("CedeSMS - Compose", LabelField.USE_ALL_WIDTH));
            
            _address = new EditField("Phone Number:", "");
            add(_address);
            
            _password = new PasswordEditField ("Password:", "");
            add(_password);
            add(new SeparatorField());
            
            _sendText = new EditField("", "");
            add(_sendText);
            
    
            _status = new EditField();
            //add(_status);
            
            addMenuItem(_sendMenuItem);
            
            if (GlobalAppProperties._islite == false) {
                addMenuItem(_showTrustedContactsSelonly);
                addMenuItem(_showContactgroupsSelonly);
            }
            addMenuItem(_showAddressBook);
            
            
        }
        
        
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            super.close();
            
        }
    }
    
    
    
    // CedeSMS Main screen - showing a list of current received messages.
    private class CedeSMSMainScreen extends MainScreen implements ListFieldCallback, MessageListener {

    //{
        
        // Constructor
        private CedeSMSMainScreen()
        {
            setTitle(new LabelField("CedeSMS v1.71 - Messages", LabelField.USE_ALL_WIDTH));
            
            //_address = new EditField("Phone Number:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);
            //add(_address);
            
            //_password = new PasswordEditField ("Password:", "");
            //add(_password);
            //add(new SeparatorField());
            
            //_sendText = new EditField("", "");
            //add(_sendText);
            // Register a listener for inbound messages.
            try {
                if (_messconnok == true) {
                    messconn.setMessageListener(this);
                }
            } catch (IOException e) {
                
            }            
    
            _status = new EditField();
                                   
            _messageListField = new ListField();
            _messageListField.setRowHeight ((_listfontnormal.getHeight()*2)+1);
            
            _messageListField.setCallback( this );
            
            add(_messageListField);
            addMenuItem(_composeMenuItem);
            
            if (GlobalAppProperties._islite == false) {
                addMenuItem(_showTrustedContacts);
                addMenuItem(_showContactgroups);
            }
            
            addMenuItem(_deleteMenuItem);
            addMenuItem(_optionsMenuItem);
            addMenuItem(_aboutMenuItem);
            addMenuItem(_enterlicenseMenuItem);
            addMenuItem(_shutdownMenuItem);
            //addMenuItem(_testMenuItem);
            
            UserSettings settings = _userSettingsscreen.getUserSettings();
            
            if (settings._showdevoptions.compareTo("yes") == 0) {
                addMenuItem(_simTCRMenuItem);
                addMenuItem(_simTCAMenuItem);
                addMenuItem(_testaddMenuItem);
            }
            
            //add(_status);
            
            // Add some sample elements to our vector array
            //_smsList.addElement(new SmsMessage("01234123456","Sample Message"));
            loadSMSlist(); 
            
            if (_messconnok == false) {
                //AddSMSItem(new SmsMessage("00000000000","CedeSMS was unable to listen for incoming SMS messages. This may be because a previous version of CedeSMS is running. Please remove any previous versions of CedeSMS."));   
            }
            
            //_smsList.addElement(new SmsMessage("01235123457","Message Body 2"));
            //_smsList.addElement(new SmsMessage("01236123458","Message Body 3"));
            //_smsList.addElement(new SmsMessage("01237123459","Message Body 4"));
            //_smsList.addElement(new SmsMessage("01238123450","Message Body 5"));
            //_smsList.addElement(new SmsMessage("01239123451","Message Body 6"));
            //_smsList.addElement(new SmsMessage("01230123452","Message Body 7"));
            
            //String strKey = GenerateKey("password", 8192);
            
            //String strPlaintext = "Hello this is a test";
            //System.out.println(strPlaintext);
            //String strEncrypted = AddCipherLayer(strKey, 10, strPlaintext, true);
            //System.out.println(strEncrypted);
            
            //String strDecrypted = AddCipherLayer(strKey, 10, strEncrypted, false);
            
            //System.out.println(strDecrypted);
        
            _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            
            
        }
        
       

        public void notifyIncomingMessage(MessageConnection conn) {
            if (conn == messconn) {
                //reader.handleMessage();               
                _receivethread = new ReceiveThread ();
                _receivethread.start();
            }
        }
        
                
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            Dialog.alert("Press the Red Hangup button to get back to the Blackberry home screen.");
            
        }
        
        public void onFocus()
        {
            Dialog.alert("Focus!");
        }
        
        
        private SmsMessage getSelectedSMS() 
        {
            int selectedIndex = _messageListField.getSelectedIndex();
            
            if ( selectedIndex == -1 ) 
            {
                return null;
            }
            
            SmsMessage smsmessage = (SmsMessage) _smsList.elementAt( selectedIndex );
            smsmessage.setUnread (false);
            _smsList.setElementAt(smsmessage, selectedIndex); 
            _persistsms.commit();
            
            return (SmsMessage) _smsList.elementAt( selectedIndex );
        }    
                
        
        public boolean invokeAction(int action)
        {        
            switch(action)
            {
                case ACTION_INVOKE: // Trackball click.
                    SmsMessage smsmessage = getSelectedSMS();
                    
                    if (smsmessage != null) {
                    
                        //UiApplication.getUiApplication().pushModalScreen(new HelloDetailsScreen());
                        //Dialog.ask(
                        LED.setState( LED.STATE_OFF );
                        
                        if (_soundplaying == true) {
                            _soundthread.stop();
                            _soundplaying = false;
                        }
                        
                        // 0 = incoming
                        // 1 = outgoing
                        if (smsmessage.getMessage().startsWith("#RC") == true || smsmessage.getMessage().startsWith("#RA") == true) {
                            
                            if (smsmessage.getMessage().startsWith("#RC") == true) {
                                if (smsmessage.getType() == 0) {
                                    pushScreen(new ViewTCRScreen(0,true));
                                    _frommessage.setText("This contact would like to setup secure messaging with you. If you accept, select 'Accept' from the main menu.");
                                }
                                
                                if (smsmessage.getType() == 1) {
                                    pushScreen(new ViewTCRScreen(1,false));
                                    _frommessage.setText("You have sent a Trusted Contact Request for secure messaging. Once you have received a Trusted Contact Acceptance you will then be ready to send encrypted messages to this recipient.");
                                }
                                
                                _lastTCRmessage = smsmessage.getMessage();
                                _lastTCRnumber = smsmessage.getAddress();
                                
                                _fromaddress.setText(smsmessage.getAddress());                                
                            }
                            
                            if (smsmessage.getMessage().startsWith("#RA") == true) {
                                if (smsmessage.getType() == 0) {
                                    pushScreen(new ViewTCRScreen(0,false));
                                    _frommessage.setText("Your Trusted Contact Request has been accepted. You can now send and receive secure text messages.");
                                }
                                
                                if (smsmessage.getType() == 1) {
                                    pushScreen(new ViewTCRScreen(1,false));
                                    _frommessage.setText("You have accepted a Trusted Contact Request. You can now send and receive secure text messages with this contact.");
                                }
                                
                                _lastTCRmessage = smsmessage.getMessage();
                                _lastTCRnumber = smsmessage.getAddress();
                                
                                _fromaddress.setText(smsmessage.getAddress());                                
                            }
                            
                        } else {
                            
                            if (smsmessage.getType() == 0) {
                                pushScreen(new ViewSMSScreen(0));
                            }
                            
                            if (smsmessage.getType() == 1) {
                                pushScreen(new ViewSMSScreen(1));
                            }
                        
                            _fromaddress.setText(smsmessage.getAddress());
                            _frommessage.setText(smsmessage.getMessage());
                        
                            // If this mobile number is in our trusted contacts then set the password automatically
                            if (_trustedContactList.doesMobileexist(smsmessage.getAddress()) == true) {
                                SingleTrustedContact tcontact = new SingleTrustedContact();
                                tcontact = _trustedContactList.getContactfromMobile(smsmessage.getAddress());                                
                                _frompassword.setText(tcontact._password);
                            }
                            
                        }                                                                    
                    }
                    return true; // We've consumed the event.
            }   
            
            return  super.invokeAction(action);
        }
        
        //////////////////////////////////////
        // ListFieldCallback methods
        //////////////////////////////////////    
        
        /**
        * Draws a row in the list of memos.
        * 
        * @param listField The ListField whose row is being drawn.
        * @param graphics The graphics context to use for drawing.
        * @param index The index of the row being drawn.
        * @param y The distance from the top of the screen where the row is being drawn.
        * @param width The width of the row being drawn.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#drawListRow(ListField,Graphics,int,int,int)
        */
        public void drawListRow( ListField listField, Graphics graphics, int index, int y, int width ) 
        {
            SmsMessage smsmessage = (SmsMessage) get( listField, index );
            
            if (smsmessage.getUnread() == true) {
                graphics.setFont(_listfontbold);
            } else {
                graphics.setFont(_listfontnormal);
            }
            
             //void drawBitmap(int x, int y, int width, int height, Bitmap bitmap, int left, int top) 
                //Draws a bitmap. 
            //_crossbitmap = Bitmap.getBitmapResource("Cross.png");
            //_openedbitmap = Bitmap.getBitmapResource("Opened.png");
            //_sendingbitmap = Bitmap.getBitmapResource("Sending.png");
            //_tickbitmap = Bitmap.getBitmapResource("Tick.png");
            //_unreadbitmap = Bitmap.getBitmapResource("Unread.png");
            //private int _type = 0; // 0 = incoming, 1 = outgoing
            //private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
            //private int _ID = 1; // record ID for updating status in the list
            
            
            if (smsmessage.getType () == 0) {
                if (smsmessage.getMessage().startsWith("#RC") == true) {                    
                    graphics.drawText("From: " + resolveContactnamewithTCR (smsmessage.getAddress (), smsmessage.getMessage()), 44, y, 0, width-44 );                    
                } else {
                    graphics.drawText("From: " + resolveContactname (smsmessage.getAddress ()), 44, y, 0, width-44 );
                }                                                                                                
            } else {
                graphics.drawText("To: " + resolveContactname (smsmessage.getAddress ()), 44, y, 0, width-44 );
            }                        
            
            graphics.setColor(Color.GRAY);
            
            if (smsmessage.getMessage().startsWith("#RC") == true || smsmessage.getMessage().startsWith("#RA") == true) {
                if (smsmessage.getMessage().startsWith("#RC") == true) {
                    if (smsmessage.getType () == 0) {
                        graphics.drawText ("You have received a Trusted Contact Request.", 44, y+_listfontnormal.getHeight(), 0, width-44);
                    } else {
                        graphics.drawText ("Trusted Contact Request.", 44, y+_listfontnormal.getHeight(), 0, width-44);
                    }
                }
                
                if (smsmessage.getMessage().startsWith("#RA") == true) {
                    if (smsmessage.getType () == 0) {
                        graphics.drawText ("Trusted Contact Request accepted.", 44, y+_listfontnormal.getHeight(), 0, width-44);
                    } else {
                        graphics.drawText ("Trusted Contact Acceptance.", 44, y+_listfontnormal.getHeight(), 0, width-44);
                    }
                }
                
            } else {
                graphics.drawText (smsmessage.getMessage(), 44, y+_listfontnormal.getHeight(), 0, width-44);
            }                        
            
            if (smsmessage.getType () == 0) {
                if (smsmessage.getUnread() == true) {
                    graphics.drawBitmap(2, y+3, 40, 33, _unreadbitmap, 0, 0);
                } else {
                    graphics.drawBitmap(2, y+3, 40, 33, _openedbitmap, 0, 0);
                }
            } else {
                if (smsmessage.getSendstatus() == 0) {
                    graphics.drawBitmap(2, y+3, 40, 33, _sendingbitmap, 0, 0);
                } 
                
                if (smsmessage.getSendstatus() == 1) {
                    graphics.drawBitmap(2, y+3, 40, 33, _tickbitmap, 0, 0);
                } 
                
                if (smsmessage.getSendstatus() == 2) {
                    graphics.drawBitmap(2, y+3, 40, 33, _crossbitmap, 0, 0);
                }               
                
            } 
            
            
            
            graphics.drawLine (0, y+(_listfontnormal.getHeight()*2), width, y+(_listfontnormal.getHeight()*2));
        }    
        
        /**
        * Retrieves the element from the specified ListField at the specified index. 
        * @param listField The ListField from which to retrieve the element.
        * @param index The index into the ListField from which to retrieve the element.
        * @return The requested element.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
        */
        public Object get( ListField listField, int index ) 
        {
            return _smsList.elementAt( index );
        }    
        
        /**
        * Returns the preferred width of the provided ListField. 
        * @param listField The ListField whose preferred width is being retrieved.
        * @return The ListField's preferred width.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
        */
        public int getPreferredWidth( ListField listField ) 
        {
            //return Display.getWidth();
            return getWidth();
        }    
        
        /**
        * Retrieves the first occurrence of the provided prefix in the list (not implemented). 
        * @param listField The ListField being searched.
        * @param prefix The prefix to search for.
        * @param start List item at which to start the search.
        * @return -1 (not implemented).
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField,String,int)
        */
        public int indexOfList( ListField listField, String prefix, int start ) 
        {
            return -1;
        }
        
    }

    // Constructor
    private CedeSMS()
    {
        
        _listfontbold = Font.getDefault();
        _listfontbold = _listfontbold.derive(Font.BOLD);
        _listfontnormal = Font.getDefault();
        
        _crossbitmap = Bitmap.getBitmapResource("Cross.png");
        _openedbitmap = Bitmap.getBitmapResource("Opened.png");
        _sendingbitmap = Bitmap.getBitmapResource("Sending.png");
        _tickbitmap = Bitmap.getBitmapResource("Tick.png");
        _unreadbitmap = Bitmap.getBitmapResource("Unread.png");        
        
        UserSettingsScreen usersettings = new UserSettingsScreen();
        UserSettings settings = new UserSettings();
        settings = usersettings.getUserSettings();
       
        
       
        try {           
            // Get our receiving port connection.
            messconn = (MessageConnection) Connector.open("sms://:" + settings._smsport);
            _messconnok = true;
        } catch (IOException e) {
            // Handle startup errors
            _messconnok = false;
        }                                        
        
        CedeSMSMainScreen screen = new CedeSMSMainScreen();
        pushScreen(screen);
    }

    public void startLED()
    {
        UserSettingsScreen usersettings = new UserSettingsScreen();
        UserSettings settings = new UserSettings();
        settings = usersettings.getUserSettings();        
        
        //LED.setColorConfiguration(500, 250, 0x00ffff00);
        LED.setColorConfiguration(settings._ledontime, settings._ledofftime, getColorvalue(settings._ledredvalue, settings._ledgreenvalue, settings._ledbluevalue));
                
        //LED.setConfiguration( 500, 250, LED.BRIGHTNESS_100 );
        LED.setState( LED.STATE_BLINKING );
    }

    public void activate()
    {
           LicenseandPasswordCheck();
    }

    public void LicenseandPasswordCheck()
    {     
        UiApplication.getUiApplication().invokeLater( new Runnable()
        {
            public void run ()
            {
                
                UserSettings mysettings = new UserSettings();
                
                _userSettingsscreen = new UserSettingsScreen();
                mysettings = _userSettingsscreen.getUserSettings();          
                
                if (mysettings._licenseentered.compareTo("yes") != 0) {
                    UiApplication.getUiApplication().pushModalScreen(new LicenseScreen());                       
                    
                    _userSettingsscreen = new UserSettingsScreen();
                    mysettings = _userSettingsscreen.getUserSettings();                                                 
                    
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                                        
                } else {
                    if (mysettings._istrial.compareTo("yes") == 0) {
                        _trialversion = true;
                        loadTrialinfo();
                    } else {
                        _trialversion = false;
                    }
                }
                
                // Now check if we need to show the password prompt screen
                Date dtCurrent = new Date();        
                long accesstime = dtCurrent.getTime();
                
                if (accesstime > _lastaccesstime) {            
                    pushScreen(new PasswordScreen());
                }     
                
            }
        });
    }

    /**
     * Update the GUI with the data just received.
     */
    private void updateStatus(final String msg)
    {
        invokeLater(new Runnable() 
        {
            public void run()
            {
                
                // Clear the string buffer.
                _statusMsgs.delete(0, _statusMsgs.length()); 
                
                _statusMsgs.append(_status.getText());
                _statusMsgs.append('\n');
                _statusMsgs.append(msg);
                _status.setText(_statusMsgs.toString());
            }
        });
    }

    

    public void ToneTest ()
    {
        UserSettingsScreen usersettings = new UserSettingsScreen();
        UserSettings settings = usersettings.getUserSettings();
                
        if (settings._playsound.compareTo("yes") == 0) {
            if (_soundplaying == false) {
               
               if (settings._repeatsound.compareTo("yes") == 0) {
                    _soundplaying = true;
                } else {
                    _soundplaying = false;
                }
               
                
                _soundstop = false;
                _soundthread = new SoundThread ();
                _soundthread.start();
            }
        }
    }

    private void addReceivedMessage(final String address, final String msg)
    {
        invokeLater(new Runnable() 
        {
            //private static final int VOLUME = 100;
            
            public void run()
            {
                AddSMSItem(new SmsMessage (CleanSMSAddress(address),msg, true));                
                //_smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg, true));
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                
                startLED();
                                    
                //if ( Alert.isAudioSupported())
                //{
                    //Alert.startAudio(TUNE, VOLUME);
                //}
                ToneTest();
                
                if (msg.startsWith("#RA") == true) {
                    // This is a trusted contact acceptance, we need to save the trusted contact acceptance.
                    SaveTrustedAcceptance(CleanSMSAddress(address),msg);
                }
            }
        });
    }
    
    private void addSamplemessage(final String address, final String msg)
    {
        invokeLater(new Runnable() 
        {
                       
            public void run()
            {
                
                /*
                 private int _type = 0; // 0 = incoming, 1 = outgoing
                private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
                private int _ID = 1; // record ID for updating status in the list
                */
                
                AddSMSItem(new SmsMessage (address,msg, false, 0, 0, 2));                
                
                //_smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg, true));
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            }
        });
    }
    
    private void addSentMessage(final String address, final String msg)
    {
        invokeLater(new Runnable() 
        {
                       
            public void run()
            {
                
                /*
                 private int _type = 0; // 0 = incoming, 1 = outgoing
                private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
                private int _ID = 1; // record ID for updating status in the list
                */
                
                AddSMSItem(new SmsMessage (address,msg, false, 1, 0, _lastsentmessageid));                
                _lastsentmessageid++;
                //_smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg, true));
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            }
        });
    }
    
    private void updateOutgoingmessagestatus (final int status)
    {
        invokeLater(new Runnable() 
        {
                       
            public void run()
            {
                SmsMessage msg;
                
                /*
                 private int _type = 0; // 0 = incoming, 1 = outgoing
                private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
                private int _ID = 1; // record ID for updating status in the list
                */
                
                //AddSMSItem(new SmsMessage (CleanSMSAddress(address),msg, true, 1, 0, _lastsentmessageid));                
                //_lastsentmessageid++;
                //_smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg, true));
                //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                
                int a = 0;
                for (a=0;a<_smsList.size();a++) {
                    msg = (SmsMessage) _smsList.elementAt(a);
                    
                    if (msg.getID() == _lastsentmessageid-1) {
                        msg.setSendstatus(status);
                        _smsList.setElementAt(msg, a);
                    }
                    
                }
                
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                
                //continue the queued sending process
                continueSend();
            }
        });
    }
    
    /**
     * Some simple formatting for a received sms message.
     */
    private void receivedSmsMessage(Message m)
    {
        String address = m.getAddress();
        String msg = null;
        
        if ( m instanceof TextMessage )
        {
            TextMessage tm = (TextMessage) m;
            msg = tm.getPayloadText();
        }
        
        //StringBuffer sb = new StringBuffer();
        //sb.append("Received:");
        //sb.append('\n');
        //sb.append("Destination:");
        //sb.append(address);
        //sb.append('\n');
        //sb.append("Data:");
        //sb.append(msg);
        //sb.append('\n');

        //updateStatus(sb.toString());
        
        //_smsList.addElement(new SmsMessage(address,msg));
        //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
        addReceivedMessage (address, msg);
    }
    
    public void loadSMSlist()
    {
            //ec341c65286855f321b35b29
            long KEY =  0xec341c65286855f3L;
            _persistsms = PersistentStore.getPersistentObject( KEY );
            _smsList = (Vector) _persistsms.getContents();
            if( _smsList == null ) {
                _smsList = new Vector();
                AddSMSItem(new SmsMessage("01234123456","#CTINLotTQPIHOhhLMMdKcAJQCUDBV/M+EV/2Xd+zEtHrrn8q0="));   
                _persistsms.setContents( _smsList );
                //persist.commit()
            }
    }
    
    public void loadTrialinfo()
    {
        long KEY =  0x286855f321b35b29L;
        _persisttrial = PersistentStore.getPersistentObject( KEY );
        _trialinfo = (TrialInfo) _persisttrial.getContents();
        if( _trialinfo == null ) {
            _trialinfo = new TrialInfo();
            
            // Set the trial expire time                        
            _persisttrial.setContents( _trialinfo );
            
            Date dtCurrent = new Date();        
            long expiretime = dtCurrent.getTime();                            
            expiretime = expiretime + (((60000) * 60) * 24) * 7;
            
            _trialinfo.expiretime = expiretime;            
            _persisttrial.commit();            
        }
    }
    
    public void clearSMSlist()
    {
        _smsList.removeAllElements();
        AddSMSItem(new SmsMessage("01234123456","#CTINLotTQPIHOhhLMMdKcAJQCUDBV/M+EV/2Xd+zEtHrrn8q0="));   
        _persistsms.commit();
        _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
    }
    
    public void AddSMSItem (SmsMessage smsmsg)
    {
        ReverseList();
        _smsList.addElement(smsmsg);
        ReverseList();
        _persistsms.commit();
    }
    
    public String resolveContactname (String strNumber)
    {
        if (_trustedContactList.doesMobileexist(strNumber) == true) {
            return _trustedContactList.resolveName(strNumber);
        } else {
            if (_contactListScreen.doesMobileexist(strNumber) == true) {
                return _contactListScreen.resolveName(strNumber);
            } else {
                return strNumber;
            }
        }                
    }
    
    public String resolveContactnamewithTCR (String strNumber, String strMessage)
    {
        if (_trustedContactList.doesMobileexist(strNumber) == true) {
            return _trustedContactList.resolveName(strNumber);
        } else {
            if (_contactListScreen.doesMobileexist(strNumber) == true) {
                return _contactListScreen.resolveName(strNumber);
            } else {
                
                if (Encryption.isOwnerinTCR(strMessage) == true) {
                    return Encryption.getTCRowner(strMessage);
                } else {
                    return strNumber;
                }                               
            }
        }                
    }
    
    public int getColorvalue(int red, int green, int blue)
    {                            
            byte [] intByte = new byte [4];
            
            intByte[0] = (byte) blue;
            intByte[1] = (byte) green;
            intByte[2] = (byte) red;
            intByte[3] = (byte) 0;
            
            int fromByte = 0;
            
            for (int i = 0; i < 4; i++)
            {
                int n = (intByte[i] < 0 ? (int)intByte[i] + 256 : (int)intByte[i]) << (8 * i);                
                fromByte += n;
            }
            
            return fromByte;        
    }
    
    public void addSendqueue(String addr, String msg)
    {
        SingleSMS mySMS = new SingleSMS(addr, msg);
        _smsqueue.addElement(mySMS);               
    }
    
    public void beginSend()
    {
        _queuepointer = 0;
        SingleSMS mySMS = new SingleSMS("", "");
        
        if (_smsqueue.size() > 0) {
            mySMS = (SingleSMS) _smsqueue.elementAt(_queuepointer);
            
            SendThreadEx newthread = new SendThreadEx (mySMS._addr, mySMS._message);
            newthread.start();
        }                
    }
    
    public void continueSend()
    {
        _queuepointer++;
        SingleSMS mySMS = new SingleSMS("", "");
        
        if (_smsqueue.size() > _queuepointer) {
            mySMS = (SingleSMS) _smsqueue.elementAt(_queuepointer);
            
            SendThreadEx newthread = new SendThreadEx (mySMS._addr, mySMS._message);
            newthread.start();
        } else {
            // Sending has finished, clear the queue
            _smsqueue.removeAllElements();
        }
    }
   
    public boolean IsExpired ()
    {
        if (_trialversion == true) {
        
            Date dtCurrent = new Date();            
            Date dtExpire = new Date(_trialinfo.expiretime);
            
            if (dtCurrent.getTime() > dtExpire.getTime()) {
                return true;
            } else {
                return false;
            }
        
        } else {
            return false;
        }
        
        //return false;
        
        /*   
           Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            
           cal.set (Calendar.AM_PM, Calendar.PM); 
           cal.set (Calendar.HOUR, 1);
           cal.set (Calendar.MINUTE, 0);
           cal.set (Calendar.SECOND, 0);
           cal.set (Calendar.DAY_OF_MONTH, 24);
           cal.set (Calendar.MONTH, Calendar.JUNE);
           cal.set (Calendar.YEAR, 2009);
           
           Date dtu = cal.getTime();
           
           
           
           Date dtCurrent = new Date ();
           
           
                      
            if (dtCurrent.getTime () >= dtu.getTime()) {
                return true;
            } else {
                return false;
            }
            
            */
    }
    
    public void ReverseList ()
    {
        Vector _backupList = new Vector();
        SmsMessage smsmessage;                
        
        //if (_smsList.size() > 1) {
        
            int a = 0;
            //for (a=_smsList.size()-1; a==0;a--) {
               // smsmessage = new SmsMessage();
                //smsmessage = (SmsMessage) _smsList.elementAt(a);
                //_backupList.addElement(smsmessage);
            //}
            
            for (a=0;a<_smsList.size();a++) {
                smsmessage = new SmsMessage();
                smsmessage = (SmsMessage) _smsList.elementAt(_smsList.size()-a-1);
                _backupList.addElement(smsmessage);
            }
            
            _smsList.removeAllElements();
            
            for (a=0;a<_backupList.size();a++) {
                smsmessage = new SmsMessage();
                smsmessage = (SmsMessage) _backupList.elementAt(a);
                _smsList.addElement(smsmessage);
            }
        //}
        
        _backupList.removeAllElements();
    }
    
    public String CleanSMSAddress (String address)
    {
        String strFirstpart = "";
        int iColonindex = 0;
        
        strFirstpart = address.substring(6);
        iColonindex = strFirstpart.indexOf(":");
        return strFirstpart.substring(0,iColonindex);
    }

    void SaveTrustedAcceptance (String strNumber, String strEncryptedMessage)
    {
        // We've received an acceptance to our trusted contact request. The message will contain an RSA Encrypted password
        // which we will use for future sending and receiving.
        SingleTrustedContact contact = new SingleTrustedContact();
        String strPrivateExp = "";
        String strPrivateMod = "";
        
        String strFilteredmessage = strEncryptedMessage.substring(3);
        String strDecryptedpassword = "";
        
        // Get the trusted contact for this number.
        if (_trustedContactList.doesMobileexist(strNumber) == true) {
            
            contact = _trustedContactList.getContactfromMobile(strNumber);
            
            strPrivateExp = contact._privatekeyexp;
            strPrivateMod = contact._privatekeymod;
            
            if (strPrivateExp.length() > 0 && strPrivateMod.length() > 0) {
                
                strDecryptedpassword = Encryption.RSADecrypt(strPrivateMod, strPrivateExp, strFilteredmessage);
                contact._password = strDecryptedpassword;
                contact._trustedstatus = "Trusted";
                
                _trustedContactList.updateContact(contact);                
                
            } else {
                Dialog.alert("You've received a Trusted Contact Acceptance, but this contact in your Address Book did not have an associated private key. The Trust Acceptance has been disregarded.");
            }
            
        } else {
            Dialog.alert("You've received a Trusted Contact Acceptance but this number is not in your Trusted Address Book. The Trust Acceptance has been disregarded.");
        }
        
        
        
        
    }

    void AcceptTrustedContactRequest (String strNumber, String strEncryptedMessage)
    {
        if (IsExpired() == true) {
            Dialog.alert(strDemomessage);
            return;
        }
        
        // We need to accept our senders trusted contact request which will contain encrypted data in the form:
        //#RC<OwnerFirstname>&<OwnerLastname>^<PublicExponent>&<PublicModulus>
        String sendtext = "";
        SingleTrustedContact updatedContact = new SingleTrustedContact();
        
        // Extract info from encrypted message;
        String strOwner = "";
        String strOwnerfirstname = "";
        String strOwnerlastname = "";
        String strPublic = "";
        String strPublicExponent = "";
        String strPublicModulus = "";
        String strFilteredmessage = strEncryptedMessage.substring(3); // strip out the #RC.
        String strDecrypted = Encryption.Instantdecrypt(strFilteredmessage); // Decrypt the encoded message
        
        strOwner = Encryption.GetDataPart(strDecrypted,1,"^");        
        strPublic = Encryption.GetDataPart(strDecrypted,2,"^");
        
        if (strOwner.compareTo("&") != 0) {
            strOwnerfirstname = Encryption.GetDataPart(strOwner, 1, "&");
            strOwnerlastname = Encryption.GetDataPart(strOwner, 2, "&");
        }
        
        strPublicExponent = Encryption.GetDataPart(strPublic,1,"&");
        strPublicModulus = Encryption.GetDataPart(strPublic,2,"&");
        
        String strRandompassword = Encryption.generateRandompassword(); // Generate a random password which will be used for secure messaging between sender and us.
        
        // Now check if we already have a trusted contact with this mobile number, if we do then it's password will be replaced with our new generated one.
        if (_trustedContactList.doesMobileexist(strNumber) == true) {
            // Yes this number is in our trusted contact list
            updatedContact = _trustedContactList.getContactfromMobile(strNumber);            
            updatedContact._password = strRandompassword; // update our trusted contact with the newly generated password.
            updatedContact._trustedstatus = "Trusted";
            _trustedContactList.updateContact(updatedContact);                        
        } else {
            
            // Ok not in our trusted list but it might be our standard blackberry contact list, check here now
            if (_contactListScreen.doesMobileexist(strNumber) == true) {
                // Yes this number is in our blackberry contact list
                updatedContact = _contactListScreen.resolveContact(strNumber);                
                updatedContact._password = strRandompassword; // update our trusted contact with the newly generated password.
                updatedContact._trustedstatus = "Trusted";
                _trustedContactList.addContact(updatedContact);
                
            } else {
                // Ok we don't have this number anywhere, so we need to create a new trusted contact using the received number
                // and owner details embedded in the rc message, and get the user to confirm the details, but not allow them to change the password.
                
                AddTrustedContactScreen addtcscreen = new AddTrustedContactScreen();
                
                updatedContact._first = strOwnerfirstname;
                updatedContact._last = strOwnerlastname;
                updatedContact._mobile = strNumber;
                updatedContact._password = strRandompassword;
                updatedContact._trustedstatus = "Trusted";
                
                addtcscreen.setContact(updatedContact);
                
                // Show the add contact screen.
                UiApplication.getUiApplication().pushModalScreen(addtcscreen);
                
                // Once we are out of the screen, get the contact details and add the new contact to the trusted contacts                
                updatedContact = addtcscreen.getContact();
                _trustedContactList.addContact(updatedContact);
                                                
            }            
        }
                
        // By the time we get here we have either updated or added the contact in our trusted contacts list with our
        // generated password, now we need to RSAEncrypt the password we generated and send it back to the sender
        sendtext = "#RA" + Encryption.RSAEncrypt(strPublicModulus, strPublicExponent, strRandompassword);
        
        System.out.println ("TCA: " + sendtext);
        
        SendThreadEx newthread = new SendThreadEx (strNumber, sendtext);
        newthread.start();
        
    }
    
    void SendTrustedContactRequest (SingleTrustedContact tcontact)
    {
        if (IsExpired() == true) {
            Dialog.alert(strDemomessage);
            return;
        }
        
        String addr = "";
        UserSettings settings = new UserSettings();
        SingleTrustedContact contact = new SingleTrustedContact();
        contact = tcontact;
        String sendtext = "";
        
        // Get the mobile number
        addr = contact._mobile;
        addr = StringUtilities.removeChars(addr, " ");
                
        if (addr.length() < 3) {
            Dialog.alert("This contact does not have a mobile number.");
            return;
        }
                
        // First we need to generate an RSA Key Pair.
        Encryption.RSAGenerateKeys();        
        
        contact._trustedstatus = "Awaiting Trust Response";
        contact._privatekeyexp = Encryption._privatekeyexp;
        contact._privatekeymod = Encryption._privatekeymod;
        
        // Update our trusted contacts list with the new status and also the private key that was generated
        // so that when we receive a response, we know how to decrypt the message because we have the private key
        _trustedContactList.updateContact(contact);
        
        // Now we need to construct the sms containing the public key, and the name of the sender
        // which would be nice.
        _userSettingsscreen = new UserSettingsScreen();
        settings = _userSettingsscreen.getUserSettings();
        
        sendtext  = settings._ownerfirstname + "&" + settings._ownerlastname + "^" + Encryption._publickeyexp + "&" + Encryption._publickeymod;
        
        // Encrypt the sending trusted contact request
        sendtext = Encryption.Instantencrypt(sendtext);
        
        // Prefix the header
        sendtext = "#RC" + sendtext;
        
        System.out.println("TCR: " + sendtext);
                
        // Now send the request
        SendThreadEx newthread = new SendThreadEx (addr, sendtext);
        newthread.start();
        
    }

    public void RSATest()
    {
            String strData = "password123";
            byte[] bData = strData.getBytes();
            byte[] bDecrypted;
                        
            RSAKeyPairGenerator  pGen = new RSAKeyPairGenerator();
            RSAKeyGenerationParameters  genParam = new RSAKeyGenerationParameters(BigInteger.valueOf(0x11), new SecureRandom(), 128, 25);

            pGen.init(genParam);
            
            AsymmetricCipherKeyPair  pair = pGen.generateKeyPair();
            
            // Get the public key
            RSAKeyParameters paramPublic = (RSAKeyParameters) pair.getPublic();            
            BigInteger biMod = paramPublic.getModulus();
            BigInteger biExp = paramPublic.getExponent();
            
            String strModulus = biMod.toString();
            String strExponent = biExp.toString();
            
            System.out.println ("Modulus (public):");
            System.out.println (strModulus);

            System.out.println ("Exponent (public):");
            System.out.println (strExponent);            
            
            System.out.println ("generated key pair.");
            
            
            RSAKeyParameters paramNewPublic = new RSAKeyParameters(false, new BigInteger(strModulus), new BigInteger(strExponent));
            
            
            RSAKeyParameters paramPrivate = (RSAKeyParameters) pair.getPrivate();
            BigInteger biModprivate = paramPrivate.getModulus();
            BigInteger biExpprivate = paramPrivate.getExponent();
            
            String strModprivate = biModprivate.toString();
            String strExpprivate = biExpprivate.toString();
            
            System.out.println ("Modulus (private):");
            System.out.println (strModprivate);

            System.out.println ("Exponent (private):");
            System.out.println (strExpprivate);                       
            
                       
            RSAEngine eng = new RSAEngine();
            
            System.out.println ("engine created");
            
            //eng.init(true, pair.getPublic());
            eng.init(true, paramNewPublic);
            
            System.out.println ("engine initialised");
            
            bData = eng.processBlock(bData, 0, bData.length);
            
            System.out.println ("rsa block processed");
            
            String strEncrypted = new String(bData);

            System.out.println ("encrypted data available");
            
            //AddSMSItem (new SmsMessage("0000",strEncrypted, true));            
            
            System.out.println(strEncrypted);
            
            eng.init(false, pair.getPrivate());
            bDecrypted = eng.processBlock(bData, 0, bData.length); 
                        
            System.out.println ("Decryption complete");
            
            String strDecrypted = new String (bDecrypted);
            
            System.out.println(strDecrypted);
    }

    
}

