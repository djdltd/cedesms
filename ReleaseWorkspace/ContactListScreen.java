/*
 * ContactListScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import javax.microedition.lcdui.Displayable;
import net.rim.device.api.util.StringUtilities;

// The pim packages.
import javax.microedition.pim.*;

public final class ContactListScreen extends MainScreen implements ListFieldCallback
{
    // Members ------------------------------------------------------------------
    private ListField _listField;   
    private ContactList _contactList;
    private Contact _contact;
    private Vector _contacts;    
    private SelectContactAction _selectContactAction;
    
    // Inner classes ------------------------------------------------------------    
    private class SelectContactAction extends MenuItem
    {
         /**
         * This class is responsible for selecting a contact from the list.
         */
        public SelectContactAction()
        {            
            super("Select Contact" , 100000, 10);
        }
        
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            if (index != -1 && !_contacts.isEmpty())
            {
                _contact = (Contact)_contacts.elementAt(_listField.getSelectedIndex());
            }
            else
            {
                _contact = null;
            }
            
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
        }
    }
        
    // Constructors -------------------------------------------------------------    
    public ContactListScreen()
    {                               
        _selectContactAction = new SelectContactAction();
        setTitle(new LabelField("Contact List" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
                
        _listField = new ListField();        
        _listField.setCallback(this);        
        add(_listField);
        
        addMenuItem(_selectContactAction);
              
        reloadContactList();
                
    }
    
    public Contact getSelectedContact()
    {
        return _contact;
    }
    
    public boolean doesMobileexist (String strMobilenumber)
    {
        int c = 0;
        int a = 0;
        boolean bmobilefound = false;
        Contact contact;
        String strphone = "";
        
        for (c=0;c<_contacts.size();c++) {
            contact = (Contact) _contacts.elementAt(c);
            
            if (contact.countValues (Contact.TEL) > 0) {
                
                for (a=0;a<contact.countValues(Contact.TEL);a++) {
                    if (contact.getAttributes (Contact.TEL, a) == Contact.ATTR_MOBILE) {
                        strphone = contact.getString (Contact.TEL, a);
                        bmobilefound = true;
                    }
                }
                
                if (bmobilefound == true) {
                    strphone = StringUtilities.removeChars(strphone, " ");
                    
                    if (strphone.endsWith(strMobilenumber.substring(4)) == true) {
                        return true;
                    }
                }            
                
            } 
        }
        
        return false;
    }
    
    public String resolveName (String strMobilenumber)
    {
        int c = 0;
        int a = 0;
        boolean bmobilefound = false;
        Contact contact;
        String strphone = "";
        
        for (c=0;c<_contacts.size();c++) {
            contact = (Contact) _contacts.elementAt(c);
            
            if (contact.countValues (Contact.TEL) > 0) {
                
                for (a=0;a<contact.countValues(Contact.TEL);a++) {
                    if (contact.getAttributes (Contact.TEL, a) == Contact.ATTR_MOBILE) {
                        strphone = contact.getString (Contact.TEL, a);
                        bmobilefound = true;
                    }
                }
                
                if (bmobilefound == true) {
                    strphone = StringUtilities.removeChars(strphone, " ");
                    
                    if (strphone.endsWith(strMobilenumber.substring(4)) == true) {
                        if (contact.countValues (Contact.NAME) > 0) {
                            String[] name = contact.getStringArray(Contact.NAME, 0);
                            boolean found = false;
                            String nameseg;
                            StringBuffer sb = new StringBuffer();
                            
                            if((nameseg = name[Contact.NAME_PREFIX]) != null)
                            {
                                sb.append(nameseg);
                                found = true;
                            }
                            
                            if((nameseg = name[Contact.NAME_GIVEN]) != null)
                            {
                                if(found)
                                {
                                    sb.append(' ');
                                }
                                sb.append(nameseg);
                                found = true;
                            }
                            
                            if((nameseg = name[Contact.NAME_FAMILY]) != null)
                            {
                                if(found)
                                {
                                    sb.append(' ');
                                }
                                
                                sb.append(nameseg);
                            }
                            
                            return sb.toString();                            
                        }
                    }
                }            
                
            } 
        }
        
        return strMobilenumber;
    }
    
    
    public SingleTrustedContact resolveContact (String strMobilenumber)
    {
        int c = 0;
        int a = 0;
        boolean bmobilefound = false;
        Contact contact;
        SingleTrustedContact scontact = new SingleTrustedContact();
        String strphone = "";
        
        for (c=0;c<_contacts.size();c++) {
            contact = (Contact) _contacts.elementAt(c);
            
            if (contact.countValues (Contact.TEL) > 0) {
                
                for (a=0;a<contact.countValues(Contact.TEL);a++) {
                    if (contact.getAttributes (Contact.TEL, a) == Contact.ATTR_MOBILE) {
                        strphone = contact.getString (Contact.TEL, a);
                        bmobilefound = true;
                    }
                }
                
                if (bmobilefound == true) {
                    strphone = StringUtilities.removeChars(strphone, " ");
                    
                    if (strphone.endsWith(strMobilenumber.substring(4)) == true) {
                        if (contact.countValues (Contact.NAME) > 0) {
                            String[] name = contact.getStringArray(Contact.NAME, 0);
                            boolean found = false;
                            String nameseg;
                                                        
                            if((nameseg = name[Contact.NAME_PREFIX]) != null)
                            {
                                scontact._title = nameseg;
                            }
                            
                            if((nameseg = name[Contact.NAME_GIVEN]) != null)
                            {
                                scontact._first = nameseg;
                            }
                            
                            if((nameseg = name[Contact.NAME_FAMILY]) != null)
                            {                                                             
                                scontact._last = nameseg;
                            }
                            
                            
                            scontact._mobile = strMobilenumber;
                            
                            return scontact;                            
                        }
                    }
                }            
                
            } 
        }
        
        return scontact;
    }
    
    
    private boolean reloadContactList()
    {
        try
        {            
            _contactList = (ContactList)PIM.getInstance().openPIMList(PIM.CONTACT_LIST, PIM.READ_ONLY);
           
           
            //Contact hasEmail = _contactList.createContact();
            //hasEmail.addString(Contact.ORG, Contact.ATTR_NONE, "");
            //hasEmail.addString(Contact.EMAIL, Contact.ATTR_HOME, "");
            
            Enumeration contactsWithEmail = _contactList.items();
            _contacts = enumToVector(contactsWithEmail);
            _listField.setSize(_contacts.size());            
            
            return true;            
        }
        catch (PIMException e)
        {
            return false;
        }
    }
    
    private Vector enumToVector(Enumeration enumeration)
    {
        Vector v = new Vector();

        if (enumeration == null)
            return v;

        while (enumeration.hasMoreElements())
        {
            v.addElement(enumeration.nextElement());
        }
        
        return v;
    }
    
    // ListFieldCallback methods ------------------------------------------------
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#drawListRow(ListField,Graphics,int,int,int)
     */
    public void drawListRow(ListField listField, Graphics graphics, int index, int y, int width) 
    {
        if(listField == _listField && index < _contacts.size())
        {
            
            Contact item = (Contact)_contacts.elementAt(index);
            
            if (item.countValues (Contact.NAME) > 0) {
                String[] name = item.getStringArray(Contact.NAME, 0);
                boolean found = false;
                String nameseg;
                StringBuffer sb = new StringBuffer();
                
                if((nameseg = name[Contact.NAME_PREFIX]) != null)
                {
                    sb.append(nameseg);
                    found = true;
                }
                
                if((nameseg = name[Contact.NAME_GIVEN]) != null)
                {
                    if(found)
                    {
                        sb.append(' ');
                    }
                    sb.append(nameseg);
                    found = true;
                }
                
                if((nameseg = name[Contact.NAME_FAMILY]) != null)
                {
                    if(found)
                    {
                        sb.append(' ');
                    }
                    
                    sb.append(nameseg);
                }
                
                graphics.drawText(sb.toString(), 0, y, 0, width);
            }
        }
    }
    
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
     */
    public Object get(ListField listField, int index)
    {
        if(listField == _listField)
        {
            // If index is out of bounds an exception will be thrown, but that's the behaviour
            // we want in that case.
            return _contacts.elementAt(index);
        }
        
        return null;
    }
    
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
     */
    public int getPreferredWidth(ListField listField) 
    {
        // Use all the width of the current LCD.
        return getWidth();
    }
 
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField , String , int)
     */   
    public int indexOfList(ListField listField, String prefix, int start) 
    {
        return -1; // Not implemented.
    }         
}
