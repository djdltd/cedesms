/*
 * AddTrustedContactScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;


import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import javax.microedition.pim.*;
/**
 * 
 */
class AddTrustedContactScreen extends MainScreen {
    
    private static final int MAX_PHONE_NUMBER_LENGTH = 32;
    
    private EditField _eftitle;
    private EditField _effirst;
    private EditField _eflast;
    private EditField _efcompany;
    private EditField _efjobtitle;
    private EditField _efemail;
    private EditField _efwork;
    private EditField _efwork2;
    private EditField _efhome;
    private EditField _efhome2;
    private EditField _efmobile;
    private PasswordEditField _efpassword;
    
    private boolean _updatedrecord = false;
    private boolean _recordsaved = false;
    
    private String _tempid = "";
    private boolean _tempidpresent = false;
    
    private SingleTrustedContact _contact;
    
    private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
    {
        public void run()
        {

            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            setLocalcontact();
            
            _recordsaved = true;
            
            if (validateForm() == true) {
                UiApplication uiapp = UiApplication.getUiApplication();
                uiapp.popScreen(uiapp.getActiveScreen());
            }                                   
        }
    };
    
    private MenuItem _cancelMenuItem = new MenuItem("Cancel", 100, 10) 
    {
        public void run()
        {       
            _recordsaved = false;
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());                                               
        }
    };

    private MenuItem _pickMenuItem = new MenuItem("Pick from Address Book", 100, 10) 
    {
        public void run()
        {

            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            ContactListScreen contactListScreen = new ContactListScreen();
            UiApplication.getUiApplication().pushModalScreen(contactListScreen);
            
            
            Contact contact;
            // Get selected contact from contact list.
            if((contact = contactListScreen.getSelectedContact()) != null)
            {
                int a = 0;
                boolean bmobilefound = false;
                String strphone = "";
                
                if (contact.countValues (Contact.TEL) > 0) {
                    
                    for (a=0;a<contact.countValues(Contact.TEL);a++) {
                        if (contact.getAttributes (Contact.TEL, a) == Contact.ATTR_MOBILE) {
                            strphone = contact.getString (Contact.TEL, a);
                            bmobilefound = true;
                        }
                    }
                    
                    if (bmobilefound == true) {
                        _efmobile.setText(strphone);
                    }
                }
                                
                 if (contact.countValues (Contact.NAME) > 0) {
                    String[] name = contact.getStringArray(Contact.NAME, 0);                            
                    String nameseg;
                                                
                    if((nameseg = name[Contact.NAME_PREFIX]) != null)
                    {
                        _eftitle.setText(nameseg);
                    }
                    
                    if((nameseg = name[Contact.NAME_GIVEN]) != null)
                    {                                
                        _effirst.setText(nameseg);
                    }
                    
                    if((nameseg = name[Contact.NAME_FAMILY]) != null)
                    {                                                             
                        _eflast.setText(nameseg);
                    }                                                                                    
                }                 
            }

        }
    };


    AddTrustedContactScreen()
    {    
        setTitle(new LabelField("Add Trusted Contact" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        //_fromaddress = new EditField("From:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);
        _eftitle = new EditField("Title: ", "");
        _effirst = new EditField("Firstname: ", "");
        _eflast = new EditField("Lastname: ", "");
        _efmobile = new EditField("Mobile:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);
        _efpassword = new PasswordEditField ("Password:", "");
        _efcompany = new EditField("Company: ", "");
        _efjobtitle = new EditField("Job Title: ", "");
        _efemail = new EditField("Email: ", "");
        _efwork = new EditField("Work: ", "");
        _efwork2 = new EditField("Work 2: ", "");
        _efhome = new EditField("Home: ", "");
        _efhome2 = new EditField("Home 2: ", "");
                
        
        add(_eftitle);
        add(_effirst);
        add(_eflast);
        add(new SeparatorField());
        add(_efmobile);
        add(_efpassword);
        add(new SeparatorField());
        add(_efcompany);
        add(_efjobtitle);
        add(_efemail);
        add(_efwork);
        add(_efwork2);
        add(_efhome);
        add(_efhome2);
        
        addMenuItem(_saveMenuItem);
        addMenuItem(_pickMenuItem);
        addMenuItem(_cancelMenuItem);
    }
    
    public SingleTrustedContact getContact()
    {
        return _contact;
    }
    
    public boolean validateForm ()
    {
        if (_effirst.getText().length() < 2) {
            Dialog.alert("Please enter a valid Firstname.");
            return false;
        }
        
        if (_eflast.getText().length() < 2) {
            Dialog.alert("Please enter a valid Lastname.");
            return false;
        }
        
        if (_efmobile.getText().length() < 5) {
            Dialog.alert("Please enter a valid Mobile number.");
            return false;
        }
        
        return true;
    }
    
    public void setContact (SingleTrustedContact contact)
    {
        _contact = contact;
        setLocalform();
        
        _tempid = _contact._id;
        _tempidpresent = true;
    }
    
    public boolean isUpdatedrecord()
    {
        return _updatedrecord;
    }
    
    public boolean isSaved()
    {
        return _recordsaved;
    }
    
    public void setLocalcontact ()
    {
        _contact = new SingleTrustedContact();        
        Date dtCurrent = new Date();
                
        if (_tempidpresent == true) {           
            _updatedrecord = true;
            _contact._id = _tempid;           
        } else {
            _updatedrecord = false;
            _contact._id = dtCurrent.toString();            
        }
        
        _contact._title = _eftitle.getText();
        _contact._first = _effirst.getText();
        _contact._last = _eflast.getText();
        _contact._mobile = _efmobile.getText();
        _contact._password = _efpassword.getText();
        _contact._company = _efcompany.getText();
        _contact._jobtitle = _efjobtitle.getText();
        _contact._email = _efemail.getText();
        _contact._work = _efwork.getText();
        _contact._work2 = _efwork2.getText();
        _contact._home = _efhome.getText();
        _contact._home2 = _efhome2.getText();        
    }
    
    public void setLocalform()
    {
        _eftitle.setText(_contact._title);
        _effirst.setText(_contact._first);
        _eflast.setText(_contact._last);
        _efmobile.setText(_contact._mobile);
        _efpassword.setText(_contact._password);
        _efcompany.setText(_contact._company);
        _efjobtitle.setText(_contact._jobtitle);
        _efemail.setText(_contact._email);
        _efwork.setText(_contact._work);
        _efwork2.setText(_contact._work2);
        _efhome.setText(_contact._home);
        _efhome2.setText(_contact._home2);        
    }
    
    public boolean onSave()
    {
        //Dialog.alert("Auto Saved!");
        setLocalcontact();
        _recordsaved = true;
        
        return true;
    }
    
    public void close()
    {
        if (validateForm() == true) {
            super.close();
        }                                
    }
    
} 
