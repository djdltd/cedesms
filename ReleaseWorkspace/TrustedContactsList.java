/*
 * TrustedContactsList.java
 *
 * � CedeSoft Ltd, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.StringUtilities;

/**
 * 
 */
class TrustedContactsList extends MainScreen implements ListFieldCallback {
    
    // Members ------------------------------------------------------------------
    private ListField _listField;   
    private Vector _contacts = new Vector();
    private Vector _encryptedcontacts = new Vector();
    private AddTrustedContactScreen _addtrustedcontactscreen;
    private PersistentObject _persist;
    private SingleTrustedContact _selectedContact;
    private String _lastAction;
    private Font _listfontnormal;
    private String _encryptedpassword = ""; // Encrypted using instantencrypt
    private boolean _encryptedpasswordpresent = false;
    private boolean _encryptedcontactsloaded = false;
    private boolean _usingencryptedcontacts = false;
    UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
    
    private MenuItem _addMenuItem = new MenuItem("Add Trusted Contact", 100, 10) 
    {
        public void run()
        {
            UserSettings mysettings = new UserSettings();
                
            _userSettingsscreen = new UserSettingsScreen();
            mysettings = _userSettingsscreen.getUserSettings();          
            
            if (mysettings._istrial.compareTo("yes") == 0) {
                if (_contacts.size() >=3) {
                    Dialog.alert("The Trial version of CedeSMS is restricted to 3 Trusted Contacts. Please contact CedeSoft for purchase information.");
                    return;
                }
            }
            
            SingleTrustedContact trustedcontact;
            _addtrustedcontactscreen = new AddTrustedContactScreen();            
            UiApplication.getUiApplication().pushModalScreen(_addtrustedcontactscreen);
            
            
            if (_addtrustedcontactscreen.isSaved() == true)
            {
                if (_addtrustedcontactscreen.isUpdatedrecord() == false) {
                    // A new trusted contact record was added
                    trustedcontact = new SingleTrustedContact();
                    trustedcontact = _addtrustedcontactscreen.getContact();
                    trustedcontact._trustedstatus = "Trusted (Manual Entry)";
                    _contacts.addElement(trustedcontact);
                    _listField.setSize(_contacts.size());
                    saveContactlist();
                    
                    //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                } else {
                    // An existing trusted contact record was updated
                }
            }
            
        }
    };
    
    private MenuItem _deleteMenuItem = new MenuItem("Delete", 100, 10) 
    {
        public void run()
        {
            if (Dialog.ask(Dialog.D_YES_NO, "Delete Trusted Contact?") == Dialog.YES) {
                int index = _listField.getSelectedIndex();
                
                _contacts.removeElementAt(index);
                
                saveContactlist();
                
                _listField.setSize(_contacts.size());
            }            
        }
    };
    
    private MenuItem _editMenuItem = new MenuItem("Edit", 100, 10) 
    {
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            SingleTrustedContact trustedcontact;
            _addtrustedcontactscreen = new AddTrustedContactScreen();
            
            trustedcontact = new SingleTrustedContact();            
            trustedcontact = (SingleTrustedContact) _contacts.elementAt(index);
            String strTrustedstatus = trustedcontact._trustedstatus;
            _addtrustedcontactscreen.setContact(trustedcontact);
            
            
            UiApplication.getUiApplication().pushModalScreen(_addtrustedcontactscreen);
            
            if (_addtrustedcontactscreen.isSaved() == true)
            {
                if (_addtrustedcontactscreen.isUpdatedrecord() == true) {
                    // A new trusted contact record was added
                    trustedcontact = new SingleTrustedContact();
                    trustedcontact = _addtrustedcontactscreen.getContact();
                    trustedcontact._trustedstatus = strTrustedstatus;
                    
                    _contacts.setElementAt(trustedcontact, index);
                    _listField.setSize(_contacts.size());
                    saveContactlist();
                    
                    //_messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                } else {
                    // An existing trusted contact record was updated
                }
            }
        }
    };
    
    private MenuItem _sendtcrMenuItem = new MenuItem("Send Trust Request", 100, 10) 
    {
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            
            if (_contacts != null) {
                if (_contacts.size() > 0) {
                    _selectedContact = new SingleTrustedContact();            
                    _selectedContact = (SingleTrustedContact) _contacts.elementAt(index);    
                }
            }
                                    
            //private SingleTrustedContact _selectedContact;
            //private String _lastAction;
            _lastAction = "sendtcr";
            
                        
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
            
        }
    };        
    
    private MenuItem _selectMenuItem = new MenuItem("Select Contact", 100, 10) 
    {
        public void run()
        {
            int index = _listField.getSelectedIndex();
            
            
            if (_contacts != null) {
                if (_contacts.size() > 0) {
                    _selectedContact = new SingleTrustedContact();            
                    _selectedContact = (SingleTrustedContact) _contacts.elementAt(index);    
                }
            }
                                    
            //private SingleTrustedContact _selectedContact;
            //private String _lastAction;
            _lastAction = "selectcontact";
            
                        
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
            
        }
    };        
    
    TrustedContactsList(String strMenuoptions) 
    {    
        _listfontnormal = Font.getDefault();
        setTitle(new LabelField("Trusted Address Book" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
                
        _listField = new ListField();        
        _listField.setCallback(this);        
        add(_listField);
        
        if (strMenuoptions.compareTo("showall") == 0) {
            addMenuItem(_addMenuItem);
            addMenuItem(_deleteMenuItem);
            addMenuItem(_editMenuItem);
            addMenuItem(_sendtcrMenuItem);
        }
        
        if (strMenuoptions.compareTo("select") == 0) {
            addMenuItem(_selectMenuItem);
        }
        
        // Load the contact list from the persistent store
        
        loadContactlist();
        
        _listField.setRowHeight ((_listfontnormal.getHeight()*2)+1);
        // Refresh the list
        _listField.setSize(_contacts.size());
        
    }
    
    public void setEncryptedpassword(String strEncryptedpassword)
    {
        _encryptedpassword = strEncryptedpassword;
        _encryptedpasswordpresent = true;
        if (_usingencryptedcontacts == true) {
            loadContactlist();
        }
        
    }
    
    public String getLastaction()
    {
        return _lastAction;
    }
    
    public SingleTrustedContact getSelectedcontact()
    {
        return _selectedContact;
    }

    public void loadContactlist ()
    {
         // Hash of "CedeSMS".
         //9504d44735f128def178e9a3a10185f1
         long KEY =  0x9504d44735f128deL;
         _persist = PersistentStore.getPersistentObject( KEY );
         
         if (_usingencryptedcontacts == true) {
                _encryptedcontacts = (Vector) _persist.getContents();
                if( _encryptedcontacts == null ) {
                    _encryptedcontacts = new Vector();
                    _persist.setContents( _encryptedcontacts );
                    //persist.commit()
                }
                
                // Now check we have a password we can use to decrypt the contacts
                if (_encryptedpasswordpresent == true) {
                    
                    String strPassword = Encryption.Instantdecrypt(_encryptedpassword);
                    int e = 0;
                    SingleTrustedContact econtact = new SingleTrustedContact();
                    // Now decrypt and transfer the encrypted contacts to our decrypted vector
                    _contacts.removeAllElements();
                    for(e=0;e<_encryptedcontacts.size();e++) {
                        econtact = (SingleTrustedContact) _encryptedcontacts.elementAt(e);                
                        econtact.DecryptContact(strPassword);                
                        _contacts.addElement(econtact);                           
                    }
                                            
                    _encryptedcontactsloaded = true;
                }   
            } else {
                _contacts = (Vector) _persist.getContents();
                if( _contacts == null ) {
                    _contacts = new Vector();
                    _persist.setContents( _contacts );
                    //persist.commit()
                }
            }
         
    }

    public void saveContactlist()
    {
        if (_usingencryptedcontacts == true) {
            if (_encryptedpasswordpresent == true) {
                // transfer the plaintext contacts to our encrypted contacts
                // and then commit the encrypted contacts to the persistant store
                String strPassword = Encryption.Instantdecrypt(_encryptedpassword);
                SingleTrustedContact contact = new SingleTrustedContact();
                int c = 0;
                _encryptedcontacts.removeAllElements();
                
                for (c=0;c<_contacts.size();c++) {
                    contact = (SingleTrustedContact) _contacts.elementAt(c);
                    contact.EncryptContact(strPassword);
                    _encryptedcontacts.addElement(contact);
                }
                
                _persist.commit();
            }
        } else {
            _persist.commit();
        }        
    }

    public void updateContact (SingleTrustedContact changedContact)
    {
        int a = 0;
        SingleTrustedContact tempContact;
        
        for (a=0;a<_contacts.size();a++) 
        {
            tempContact = new SingleTrustedContact();            
            tempContact = (SingleTrustedContact) _contacts.elementAt(a);
            
            if (tempContact._id.compareTo(changedContact._id) == 0) {
                _contacts.setElementAt(changedContact, a);
                
                saveContactlist();
                return;
            }
        }               
    }
    
    public void addContact (SingleTrustedContact newContact)
    {
        Date dtCurrent = new Date();
        
        SingleTrustedContact contact = new SingleTrustedContact();
        contact = newContact;                                       
        contact._id = dtCurrent.toString();
        
         _contacts.addElement(contact);
        saveContactlist();
    }

    public void clearContacts()
    {
        _contacts.removeAllElements();
        saveContactlist();
        _listField.setSize(_contacts.size());
    }

    public boolean doesMobileexist (String strMobilenumber)
    {
        int c = 0;
        
        SingleTrustedContact contact = new SingleTrustedContact();
        
        for (c=0;c<_contacts.size();c++) {
            contact = (SingleTrustedContact) _contacts.elementAt(c);
            
            contact._mobile = StringUtilities.removeChars(contact._mobile, " ");
            
            if (contact._mobile.endsWith(strMobilenumber.substring(4)) == true) {
                return true;
            }
        }
        
        return false;
    }

    public SingleTrustedContact getContactfromMobile (String strMobilenumber)
    {
        int c = 0;        
        SingleTrustedContact contact = new SingleTrustedContact();
        
        for (c=0;c<_contacts.size();c++) {
            contact = (SingleTrustedContact) _contacts.elementAt(c);
            
            contact._mobile = StringUtilities.removeChars(contact._mobile, " ");
            
            if (contact._mobile.endsWith(strMobilenumber.substring(4)) == true) {
                return contact;
            }
        }
        
        return contact;
    }
    
    public SingleTrustedContact getContactfromIndex (int index)
    {
        SingleTrustedContact myContact = new SingleTrustedContact();
        myContact = (SingleTrustedContact) _contacts.elementAt(index);
        
        return myContact;
    }
    
    public int getNumcontacts()
    {
        return _contacts.size();               
    }

    public String resolveName (String strMobilenumber)
    {
        int c = 0;
        String strFullname = "";
        
        SingleTrustedContact contact = new SingleTrustedContact();
        
        for (c=0;c<_contacts.size();c++) {
            contact = (SingleTrustedContact) _contacts.elementAt(c);
            
            contact._mobile = StringUtilities.removeChars(contact._mobile, " ");
            
            if (contact._mobile.endsWith(strMobilenumber.substring(4)) == true) {
                strFullname = contact._first + " " + contact._last;
                return strFullname.trim();
            }
        }
        
        return strMobilenumber;
    }

    public void drawListRow(ListField listField, Graphics graphics, int index, int y, int width) 
    {
        if(listField == _listField && index < _contacts.size())
        {                
                SingleTrustedContact item = (SingleTrustedContact) _contacts.elementAt(index);
            
                
                String Fullname = "";
                if (item._first.length() > 0) {
                    Fullname+=item._first;
                }
                
                if (item._last.length() > 0) {
                    if (item._first.length() > 0) {
                        Fullname+=" ";                        
                    }
                    Fullname+=item._last;
                }                        
                                        
                //graphics.drawText(Fullname, 0, y, 0, width);  
                graphics.drawText(Fullname, 0, y, 0, width);
                graphics.setColor(Color.GRAY);     
                graphics.drawText (item._trustedstatus, 0, y+_listfontnormal.getHeight(), 0, width);
                graphics.drawLine (0, y+(_listfontnormal.getHeight()*2), width, y+(_listfontnormal.getHeight()*2));
        }
    }

    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
     */
    public Object get(ListField listField, int index)
    {
        if(listField == _listField)
        {
            // If index is out of bounds an exception will be thrown, but that's the behaviour
            // we want in that case.
            return _contacts.elementAt(index);
        }
        
        return null;
    }
    
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
     */
    public int getPreferredWidth(ListField listField) 
    {
        // Use all the width of the current LCD.
        return getWidth();
    }
 
    /**
     * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField , String , int)
     */   
    public int indexOfList(ListField listField, String prefix, int start) 
    {
        return -1; // Not implemented.
    }

} 
