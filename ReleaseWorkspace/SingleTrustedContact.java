/*
 * SingleTrustedContact.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.util.Persistable;


/**
 * 
 */
class SingleTrustedContact implements Persistable {
    
    public String _id = "";
    public String _title = "";
    public String _first = "";
    public String _last = "";
    public String _company = "";
    public String _jobtitle = "";
    public String _email = "";
    public String _work = "";
    public String _work2 = "";
    public String _home = "";
    public String _home2 = "";
    public String _mobile = "";
    public String _password = "";
    public String _trustedstatus = "";
    public String _privatekeymod = "";
    public String _privatekeyexp = "";
    public Vector _memberships = new Vector();
    
    SingleTrustedContact() 
    {    
    
    }
    
    public boolean isGroupmember(String strGroupname)
    {
        int a = 0;
        String strCurrentgroup = "";
        for (a=0;a<_memberships.size();a++) {
            strCurrentgroup = (String) _memberships.elementAt(a);
            
            if (strCurrentgroup.compareTo(strGroupname) == 0) {
                return true;
            }
        }
        
        return false;
    }
    
    void addMembership(String strGroupname)
    {
        _memberships.addElement(strGroupname);
    }
    
    void deleteMembership(String strGroupname)
    {
        int a = 0;
        String strCurrentgroup = "";
        for (a=0;a<_memberships.size();a++) {
            strCurrentgroup = (String) _memberships.elementAt(a);
            
            if (strCurrentgroup.compareTo(strGroupname) == 0) {
                _memberships.removeElementAt(a);
                return;
            }
        }        
    }
    
    void EncryptContact (String strPassword)
    {
        _id = Encryption.EncryptTextPFX(strPassword, _id);
        _title = Encryption.EncryptTextPFX(strPassword, _title);
        _first = Encryption.EncryptTextPFX(strPassword, _first);
        _last = Encryption.EncryptTextPFX(strPassword, _last);
        _company = Encryption.EncryptTextPFX(strPassword, _company);
        _jobtitle = Encryption.EncryptTextPFX(strPassword, _jobtitle);
        _email = Encryption.EncryptTextPFX(strPassword, _email);
        _work = Encryption.EncryptTextPFX(strPassword, _work);
        _work2 = Encryption.EncryptTextPFX(strPassword, _work2);
        _home = Encryption.EncryptTextPFX(strPassword, _home);
        _home2 = Encryption.EncryptTextPFX(strPassword, _home2);
        _mobile = Encryption.EncryptTextPFX(strPassword, _mobile);
        _password = Encryption.EncryptTextPFX(strPassword, _password);         
        _trustedstatus = Encryption.EncryptTextPFX(strPassword, _trustedstatus);         
        _privatekeymod = Encryption.EncryptTextPFX(strPassword, _privatekeymod);                
        _privatekeyexp = Encryption.EncryptTextPFX(strPassword, _privatekeyexp);      
    }
    
    void DecryptContact (String strPassword)
    {
        _id = Encryption.DecryptTextPFX(strPassword, _id);
        _title = Encryption.DecryptTextPFX(strPassword, _title);
        _first = Encryption.DecryptTextPFX(strPassword, _first);
        _last = Encryption.DecryptTextPFX(strPassword, _last);
        _company = Encryption.DecryptTextPFX(strPassword, _company);
        _jobtitle = Encryption.DecryptTextPFX(strPassword, _jobtitle);
        _email = Encryption.DecryptTextPFX(strPassword, _email);
        _work = Encryption.DecryptTextPFX(strPassword, _work);
        _work2 = Encryption.DecryptTextPFX(strPassword, _work2);
        _home = Encryption.DecryptTextPFX(strPassword, _home);
        _home2 = Encryption.DecryptTextPFX(strPassword, _home2);
        _mobile = Encryption.DecryptTextPFX(strPassword, _mobile);
        _password = Encryption.DecryptTextPFX(strPassword, _password);   
        _trustedstatus = Encryption.DecryptTextPFX(strPassword, _trustedstatus);      
        _privatekeymod = Encryption.DecryptTextPFX(strPassword, _privatekeymod);            
        _privatekeyexp = Encryption.DecryptTextPFX(strPassword, _privatekeyexp);            
    }
    
} 
