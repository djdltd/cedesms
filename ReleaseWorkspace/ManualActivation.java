/*
 * ManualActivation.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package CedeSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;


/**
 * 
 */
class ManualActivation  extends MainScreen {
    
    private EditField _requestcode;
    private EditField _responsecode;
    private EditField _reconkey;
    private String _prodkey = "";
    private Licensing _lic = new Licensing();
    
    private String _actresult;
    
    private MenuItem _activateMenuItem = new MenuItem("Activate", 100, 10) 
    {
        public void run()
        {
            String strRecoveredkey = "";
            boolean actresultok = false;
            
            if (_responsecode.getText().length() > 5) {
                                        
                strRecoveredkey = _lic.ResponseNumericToKey(_responsecode.getText(), 147);            
                System.out.println("Recovered Key, SUCCESS mode: " + strRecoveredkey);
                System.out.println("_prodkey: " + _prodkey);
                
                //if (_prodkey.indexOf(strRecoveredkey) != -1) {
                if (strRecoveredkey.compareTo(_prodkey) == 0) {
                    System.out.println("Act result was SUCCESS.");
                    _actresult = "SUCCESS";
                    actresultok = true;
                }
                
                
                strRecoveredkey = _lic.ResponseNumericToKey(_responsecode.getText(), 177);            
                System.out.println("Recovered Key, FAILED mode: " + strRecoveredkey);
                //if (_prodkey.indexOf(strRecoveredkey) != -1) {
                if (strRecoveredkey.compareTo(_prodkey) == 0) {
                    System.out.println("Act result was FAILED.");
                    _actresult = "FAILED";
                    actresultok = true;
                }
                
                
                strRecoveredkey = _lic.ResponseNumericToKey(_responsecode.getText(), 159);            
                System.out.println("Recovered Key, DENIED mode: " +  strRecoveredkey);
                //if (_prodkey.indexOf(strRecoveredkey) != -1) {
                if (strRecoveredkey.compareTo(_prodkey) == 0) {
                    System.out.println("Act result was DENIED.");
                    _actresult = "DENIED";
                    actresultok = true;
                }
                
                System.out.println("Finished manual activation recovery");
                
                if (actresultok == true) {
                    UiApplication uiapp = UiApplication.getUiApplication();
                    uiapp.popScreen(uiapp.getActiveScreen());                        
                } else {
                    Dialog.alert("The response code you have entered is not valid. Please ensure you have entered it correctly.");
                }
                
            } else {
                
                Dialog.alert("Please enter a valid response code.");
                
            }
        }
    };
        
        
        
    ManualActivation(String strProductkey) {    
        
        _prodkey = strProductkey;
                
        setTitle(new LabelField("Manual Product Activation" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        String strRequestcode = _lic.GenerateRequestCode(_prodkey);
        
        _requestcode = new EditField("", strRequestcode);
        _responsecode = new EditField("", "");
        _reconkey = new EditField("", "");
        
        add(new LabelField("Automatic activation was not possible from your handset. To activate manually, go to http://www.cedesoft.com/ManualActivation.aspx from an internet connected PC and enter the following request code:"));
        add(_requestcode);
        add(new SeparatorField());
        add(new LabelField("You will then be given a response code. Please enter this reponse code here:"));
        add(_responsecode);
        add(new SeparatorField());
        add(new LabelField("Once you have entered the response code, select 'Activate' from the Blackberry menu."));
        //add(_reconkey);
        
        addMenuItem(_activateMenuItem);
        
    }
    
    public String getActivationresult()
    {
        return _actresult;
    }
} 
